export const environment = {
  production: true,
  apiUrl: 'https://project-gamma-backend.herokuapp.com/api/v1'
};
