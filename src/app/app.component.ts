import { Component, OnInit } from '@angular/core';
import { ApiService } from './shared/services/api.service';

@Component({
  selector: 'gamma-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'gamma';

  constructor(
    private apiService: ApiService
  ) {}

  ngOnInit() {
    this.apiService.populate();
  }
}
