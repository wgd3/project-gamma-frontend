import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Player } from '../shared/models/player';
import { Observable } from '../../../node_modules/rxjs';

@Component({
  selector: 'gamma-marketplace',
  templateUrl: './marketplace.component.html',
  styleUrls: ['./marketplace.component.css']
})
export class MarketplaceComponent implements OnInit {

  currentPlayer: Player = new Player();
  merch: any[] = [];

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.loadMerch();
  }

  loadMerch() {
    this.apiService.get('/merch').subscribe(
      resp => {
        this.merch = resp.items;
      }
    );
  }

  buyItem(id: number) {
    const payload = {};
    this.apiService.post(`/merch/purchase/${id}`, payload).subscribe(
      resp => {
        console.log(`Successfully bought item!`);
        const item = this.merch.filter(x => x.id === id)[0];
        this.currentPlayer.available_coins -= item.cost;
        // this.currentPlayer.inventory.append()
        this.apiService.refreshUser();
      },
      (err) => {
        console.log(JSON.stringify(err, null, 2));
      },
      () => {}
    );
  }

  alreadyInInventory(id: number) {
    return this.currentPlayer.inventory.filter(x => x.merch_id === id).length;
  }
}
