import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Player } from '../../shared/models/player';
import { Team } from '../../shared/models/team';
import { forkJoin } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'gamma-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  currentPlayer: Player = new Player();
  team: Team = new Team();
  mobs: any[];

  // stats
  mobCountInLastThirtyDays = 0;
  mobCountInLastWeek = 0;

  loading = false;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.loading = true;
    forkJoin(
      this.apiService.get(`/teams/${this.currentPlayer.team_id}`),
      this.apiService.get(`/mobs`)
    ).subscribe(
      ([teamResp, mobResp]) => {
        this.team = teamResp;
        this.mobs = mobResp.items;
        // this.mobs.forEach(
        //   (mob) => {
        //     // init array
        //     mob['intervals'] = [];
        //     this.apiService.get(`/intervals/mob/${mob.id}`).subscribe(
        //       (resp) => {
        //         mob['intervals'] = resp.items;
        //       }
        //     );
        //   }
        // );
      },
      (err) => {},
      () => {
        console.log(`Finished loading landing page data`);
        this.loading = false;
        this.mobCountInLastThirtyDays = this.getMobCountOverTimespan(30);
        this.mobCountInLastWeek = this.getMobCountOverTimespan(7);
      }
    );
    // this.loadTeam();

  }

  loadTeam() {
    this.apiService.get(`/teams/${this.currentPlayer.team_id}`).subscribe(
      resp => {
        this.team = resp;
      }
    );
  }

  getMobCountOverTimespan(days: number): number {
    let recent_mobs = 0;
    const latestDate = moment().subtract(days, 'days');
    console.log(`Comparing against date: ${latestDate.toISOString()}`);
    this.mobs.forEach(
      mob => {
        const createdDate = moment.utc(mob.created);
        if (createdDate.isAfter(latestDate) === true) {
          recent_mobs += 1;
        }
      }
    );

    return recent_mobs;
  }

}
