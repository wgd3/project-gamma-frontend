import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'gamma-dashboard-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  dashboard_link_categories = [
    'Game',
    'Economy',
    'User Management',
  ];
  dashboard_links = [
    { text: 'Players', icon: 'users', category: 'User Management', path: 'players'},
    { text: 'Mobs', icon: 'grid', category: 'Game', path: 'mobs'},
    // { text: 'Team', icon: 'briefcase', category: 'User Management', path: 'teams'},
    { text: 'Classes', icon: 'github', category: 'Game', path: 'classes'},
    { text: 'Bounties', icon: 'box', category: 'Game', path: 'bounties'},
    { text: 'Gifts', icon: 'gift', category: 'Economy', path: 'gifts'},
    { text: 'Media', icon: 'camera', category: 'Game', path: 'media'},
    { text: 'Bank', icon: 'dollar-sign', category: 'Economy', path: 'bank'},
    { text: 'Marketplace', icon: 'shopping-cart', category: 'Economy', path: 'marketplace'},
    { text: 'Levels', icon: 'chevrons-up', category: 'Economy', path: 'levels'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
