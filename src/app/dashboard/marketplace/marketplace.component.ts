import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '../../../../node_modules/@angular/forms';

@Component({
  selector: 'gamma-marketplace',
  templateUrl: './marketplace.component.html',
  styleUrls: ['./marketplace.component.css']
})
export class MarketplaceComponent implements OnInit {

  merch: any[] = [];

  merchForm: FormGroup;

  constructor(
    private apiService: ApiService,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.loadMerch();
    this.buildForm();
  }

  submitNewMerch() {
    const payload = {
      'name': this.fName.value,
      'cost': this.fCost.value,
      'description': this.fDesc.value,
      'image_id': this.fImage.value
    };
    this.apiService.post('/merch', payload).subscribe(
      resp => {
        console.log(`Created new merch!`);
      },
      (err) => {},
      () => {
        this.loadMerch();
      }
    );
  }

  open(template) {
    this.modalService.open(template).result.then(
      (result) => {},
      (result) => {
        // make sure form is reset
        this.merchForm.reset();
        // this.inviteModalStatusMessage = '';
        // this.showSpinner = false;
        console.log(`Closed modal and reset form`);
      }
    );
  }

  loadMerch() {
    this.apiService.get('/merch').subscribe(
      resp => {
        this.merch = resp.items;
      }
    );
  }

  buildForm() {
    this.merchForm = this.fb.group({
      name: [null, [
        Validators.required
      ]],
      cost: [null, [
        Validators.required,
      ]],
      description: [null, []],
      image: [null, []]
    });
  }

  get fName() { return this.merchForm.get('name'); }
  get fCost() { return this.merchForm.get('cost'); }
  get fDesc() { return this.merchForm.get('description'); }
  get fImage() { return this.merchForm.get('image'); }
}
