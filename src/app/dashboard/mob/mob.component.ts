import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Player } from '../../shared/models/player';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'gamma-mob',
  templateUrl: './mob.component.html',
  styleUrls: ['./mob.component.css']
})
export class MobComponent implements OnInit {

  currentPlayer: Player = new Player();

  mobs: any[] = [];

  constructor(
    private apiService: ApiService,
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.loadData();
  }

  loadData() {
    // collect all mob observables
    let reqs = [];
    this.apiService.get(`/mobs`).subscribe(
      resp => {
        // inject collapsed state for table
        resp.items.forEach(mob => {
          mob['collapsed'] = true;
          mob['intervals'] = [];
          reqs.push(this.apiService.get(`/intervals/mob/${mob.id}`))
          this.mobs.push(mob);
        });
      },
      (err) => {},
      () => {
        // loaded all mobs, now get all intervals
        forkJoin(reqs).subscribe(
          (resp: any[]) => {
            // for each /intervals/mob/:id request
            resp.forEach(intervalResp => {
              if (intervalResp.items.length > 0) {
                // get the mob id from the first interval
                const mob_id = intervalResp.items[0].mob_id;
                // console.log(`Updating mob ${mob_id}`);
                // get mob from this.mobs array
                let mobItem = this.mobs.filter(m => +m.id === +mob_id)[0];
                // console.log(`Adding intervals to mob ${mobItem.id}`);
                mobItem['intervals'] = intervalResp.items;
              }
            });
          }
        );
      }
    )
  }

}
