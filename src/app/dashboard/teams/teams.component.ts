import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Team } from '../../shared/models/team';
import { Player } from '../../shared/models/player';

@Component({
  selector: 'gamma-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  currentPlayer: Player = new Player();
  team: Team = new Team();

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.loadData();
  }

  loadData() {
    this.apiService.get(`/teams/${this.currentPlayer.team_id}`).subscribe(
      (resp) => {
        this.team = resp;
      },
      (err) => {},
      () => {
        console.log(`Finished loading team`);
      }
    );
  }
}
