import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { FormBuilder, FormGroup, Validators } from '../../../../node_modules/@angular/forms';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { Player } from '../../shared/models/player';
import { Team } from '../../shared/models/team';
import { map } from '../../../../node_modules/rxjs/operators';
import * as shape from 'd3-shape';

@Component({
  selector: 'gamma-level',
  templateUrl: './level.component.html',
  styleUrls: ['./level.component.css']
})
export class LevelComponent implements OnInit {

  currentPlayer: Player = new Player();
  currentTeam: Team = new Team();
  levels$;
  levels: any[] = [];
  chartData: any[] = [];

  // form vars
  levelForm: FormGroup;
  showSpinner = false;
  formStatusMessage = '';

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = false;
  xAxisLabel = 'Level';
  showYAxisLabel = false;
  yAxisLabel = 'Total XP';
  timeline = false;
  // curve: shape.CurveGenerator;
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  // line, area
  autoScale = true;
  view: any[] = [700, 400];

  constructor(
    private apiService: ApiService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      },
      (err) => {},
      () => {
        this.apiService.get(`/teams/${this.currentPlayer.team_id}`).subscribe(
          resp => {
            this.currentTeam = resp;
          }
        );
      }
    );
    // this.levels$ = this.apiService.get(`/level_xp`)
                                  // .map(res => res);

    this.loadLevels();
    this.buildForm();
  }



  loadLevels() {
    this.levels = [];
    this.apiService.get(`/level_xp`).subscribe(
      resp => {
        this.levels = resp.items;
        this.levels.sort((a, b) => {
          if (a.level < b.level) {
            return -1;
          }
          if (a.level > b.level) {
            return 1;
          }
          return 0;
        });
      },
      (err) => {},
      () => {
        const seriesData = [
          {
            name: 'Level 1',
            value: 0
          }
        ];
        let totalXp = 0;
        for (let i = 0; i < this.levels.length; i++) {
          let previousLevel;
          if (i === 0) {
            previousLevel = 1;
          } else {
            previousLevel = this.levels[i - 1].level;
          }
          const levelCount = +this.levels[i].level - +previousLevel;
          const newXp = levelCount * +this.levels[i].req_xp;
          const newDataPoint = {
            name: `Level ${this.levels[i].level}`,
            value: totalXp + newXp
          };
          totalXp += newXp;
          seriesData.push(newDataPoint);
        }
        console.log(`series data: ${JSON.stringify(seriesData)}`);
        this.chartData.push({
          name: 'XP',
          series: seriesData
        });
      }
    );
  }

  buildForm() {
    this.levelForm = this.fb.group({
      level: [null, [
        Validators.required,
        Validators.min(2)
      ]],
      req_xp: [null, [
        Validators.required,
        Validators.min(1)
      ]],
      coin_reward: [null, []]
    });
  }

  get fLevel() { return this.levelForm.get('level'); }
  get fReqXp() { return this.levelForm.get('req_xp'); }
  get fCoinReward() { return this.levelForm.get('coin_reward'); }

  open(modelTemplate) {
    this.modalService.open(modelTemplate).result.then(
      (result) => {},
      (result) => {
        // make sure form is reset
        this.levelForm.reset();
        this.formStatusMessage = '';
        this.showSpinner = false;
        console.log(`Closed modal and reset form`);
      }
    );
  }

  submitLevelForm () {
    if (this.levelForm.valid &&
        (this.levelForm.touched || this.levelForm.dirty)) {
          console.log(`Form values: ${JSON.stringify(this.levelForm.value, null, 2)}`);
          const payload = {
            'level': this.fLevel.value,
            'req_xp': this.fReqXp.value,
            'coin_reward': this.fCoinReward.value
          };
          this.showSpinner = true;
          this.formStatusMessage = 'Submitting...';
          this.apiService.post(`/level_xp`, payload).subscribe(
            resp => {
              console.log(`Created new level mapping!`);
              this.formStatusMessage = 'Success!';
              this.loadLevels();
            },
            (err) => {
              console.log(err);
              this.formStatusMessage = 'Error submitting';
            },
            () => {
              this.showSpinner = false;
            }
          );
        }
  }

  editLevel(level) {

  }

  deleteLevel(level) {

  }

}
