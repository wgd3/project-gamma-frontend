import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { PlayersComponent } from './players/players.component';
import { TeamsComponent } from './teams/teams.component';
import { AdminGuard } from '../shared/admin.guard';
import { LandingComponent } from './landing/landing.component';

const routes: Routes = [
  // { path: '', component: LandingComponent, outlet: 'dashboard-outlet' },
  // { path: 'players', component: PlayersComponent, outlet: 'dashboard-outlet' },
  // { path: 'teams', component: TeamsComponent, outlet: 'dashboard-outlet' },
  {
    path: '',
    component: DashboardComponent,
    // canActivate: AdminGuard,
    // children: [
    //   {
    //     path: '',
        // canActivate: AdminGuard,
    children: [
      { path: 'players', component: PlayersComponent },
      { path: 'teams', component: TeamsComponent },
      { path: '', component: LandingComponent }
    ]
    //   }
    // ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
