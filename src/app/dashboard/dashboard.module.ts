import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SharedModule } from '../shared/shared.module';
import { PlayersComponent } from './players/players.component';
import { TeamsComponent } from './teams/teams.component';
import { LandingComponent } from './landing/landing.component';
import { RouterModule, UrlSerializer } from '../../../node_modules/@angular/router';
import { CleanUrlSerializer } from '../shared/url.serializer';
import { FormsModule, ReactiveFormsModule } from '../../../node_modules/@angular/forms';
import { BankComponent } from './bank/bank.component';
import { MarketplaceComponent } from './marketplace/marketplace.component';
import { GiftComponent } from './gift/gift.component';
import { BountiesComponent } from './bounties/bounties.component';
import { LevelComponent } from './level/level.component';
import { MobComponent } from './mob/mob.component';

import {NgxChartsModule} from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { MediaComponent } from './media/media.component';
import { ClassComponent } from './class/class.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([]),
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    NgbModule
  ],
  declarations: [
    DashboardComponent,
    SidebarComponent,
    PlayersComponent,
    TeamsComponent,
    LandingComponent,
    BankComponent,
    MarketplaceComponent,
    GiftComponent,
    BountiesComponent,
    LevelComponent,
    MobComponent,
    MediaComponent,
    ClassComponent
  ],
  providers: [
    {
      provide: UrlSerializer,
      useClass: CleanUrlSerializer
    }
  ]
})
export class DashboardModule { }
