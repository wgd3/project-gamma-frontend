import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '../../../../node_modules/@angular/forms';
import { Player } from '../../shared/models/player';
import { Team } from '../../shared/models/team';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'gamma-gift',
  templateUrl: './gift.component.html',
  styleUrls: ['./gift.component.css']
})
export class GiftComponent implements OnInit {

  currentPlayer: Player = new Player();
  currentTeam: Team = new Team();
  gifts: any[] = [];

  giftForm: FormGroup;
  giftModalRef: any;
  showSpinner = false;
  giftFormStatusMessage = '';

  constructor(
    private apiService: ApiService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.apiService.get(`/teams/${this.currentPlayer.team_id}`).subscribe(
      (team: Team) => {
        this.currentTeam = team;
      },
      (err) => {},
      () => {}
    );
    this.loadGifts();
    this.buildForm();
  }

  loadGifts() {
    this.apiService.get(`/gifts`).subscribe(
      resp => {
        this.gifts = resp.items;
      }
    );
  }

  buildForm() {
    this.giftForm = this.fb.group({
      name: [null, [
        Validators.required
      ]],
      description: [null, [

      ]],
      xp_value: [null, [
        Validators.required,
      ]],
      players: this.fb.array([]),
    });
  }

  get fName() { return this.giftForm.get('name'); }
  get fDescription() { return this.giftForm.get('description'); }
  get fXpValue() { return this.giftForm.get('xp_value'); }
  get fPlayers() { return this.giftForm.get('players'); }

  playerChange(player_id: number, isChecked: boolean) {
    const playerFormArray = <FormArray>this.giftForm.controls.players;

    if (isChecked) {
      playerFormArray.push(new FormControl(player_id));
    } else {
      const idx = playerFormArray.controls.findIndex(x => x.value === player_id);
      playerFormArray.removeAt(idx);
    }

    console.log(`${this.fPlayers.value.length} players are selected`);
    this.fPlayers.value.forEach(x => console.log(`Player ${x} is checked`));
  }

  submitGiftForm() {
    if ((this.giftForm.valid) &&
        (this.giftForm.dirty || this.giftForm.touched)) {
        if (this.fPlayers.value.length === 0) {
          console.log(`Must select at least 1 player`);
          return false;
        }
        const payload = {
          'name': this.fName.value,
          'description': this.fDescription.value,
          'xp_value': this.fXpValue.value,
          'players': []
        };
        this.fPlayers.value.forEach(x => payload.players.push(x));
        this.apiService.post(`/gifts`, payload).subscribe(
          resp => {
            console.log(`Successfully submitted gift!`);
            this.loadGifts();
          },
          (err) => { console.log(JSON.stringify(err)); },
          () => {}
        );
    }
  }

  openGiftModal(modalTemplate) {
    this.giftModalRef = this.modalService.open(modalTemplate);
  }
}
