import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Player } from '../../shared/models/player';
import { Team } from '../../shared/models/team';

@Component({
  selector: 'gamma-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.css']
})
export class BankComponent implements OnInit {

  currentPlayer: Player = new Player();
  teamPlayers: Player[] = [];
  team: Team = new Team();

  // bank stats
  teamGdp = 0;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.loadTeam();
  }

  loadTeam() {
    console.log(`Loading team data`);
    this.apiService.get(`/teams/${this.currentPlayer.team_id}`).subscribe(
      resp => {
        this.teamGdp = 0; // resetting here since loadTeam() is called multiple times
        this.teamPlayers = resp.players;
        this.teamPlayers.forEach((player) => {
          if (player.coins.length > 0) {
            this.teamGdp += player.coins.length;
          }
        });
        this.team = resp;
      },
      (err) => {},
      () => {}
    );
  }

  grantCoin(player_id: number) {
    if (this.currentPlayer.id === player_id) {
      this.currentPlayer.available_coins += 1;
    }

    this.apiService.post(`/coins/new/${player_id}`, {}).subscribe(
      resp => {
        console.log(`Added a coin`);
        this.loadTeam();
      },
      (err) => {
        console.log(JSON.stringify(err));
      }
    );
  }

  burnCoin(player_id: number) {
    if (this.currentPlayer.id === player_id) {
      this.currentPlayer.available_coins -= 1;
    }

    this.apiService.get(`/coins/burn/${player_id}`).subscribe(
      resp => {
        console.log(`Redeemed a coin for player ${player_id}`)
        this.loadTeam();
      },
      (err) => {
        console.log(JSON.stringify(err));
      }
    );
  }

}
