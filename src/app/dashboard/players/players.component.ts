import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Player } from '../../shared/models/player';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '../../../../node_modules/@angular/forms';
import { Team } from '../../shared/models/team';
import { templateJitUrl } from '../../../../node_modules/@angular/compiler';

@Component({
  selector: 'gamma-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

  currentPlayer: Player = new Player();
  currentTeam: Team = new Team();
  players: Player[] = [];

  inviteForm: FormGroup;
  showSpinner = false;
  inviteModalStatusMessage = '';

  // player delete confirmation modal
  deleteModalRef: any;
  deleteModalPlayer: any;
  deleteModalStatusMessage = '';

  constructor(
    private apiService: ApiService,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.loadData();
    this.buildInviteForm();
  }

  loadData() {
    this.players = [];
    this.apiService.get(`/teams/${this.currentPlayer.team_id}`).subscribe(
      (team: Team) => {
        this.currentTeam = team;
      }
    );
    // backend only returns players associated with the current team
    this.apiService.get('/players').subscribe(
      (resp: Response) => {
        resp['items'].forEach(
          (player: Player) => {
            this.players.push(player);
          }
        );
      },
      (err) => {},
      () => {
        console.log(`Finished loading players`);
      }
    );
  }

  open(template) {
    this.modalService.open(template).result.then(
      (result) => {},
      (result) => {
        // make sure form is reset
        this.inviteForm.reset();
        this.inviteModalStatusMessage = '';
        this.showSpinner = false;
        console.log(`Closed modal and reset form`);
      }
    );
  }

  launchConfirmModal(player, template) {
    this.deleteModalPlayer = player;
    this.deleteModalRef = this.modalService.open(template, {size: 'sm'});
    this.deleteModalRef.result.then(
      (result) => {},
      (reason) => {
        console.log(`Dismissed modal dialog`);
        this.deleteModalPlayer = null;
        this.deleteModalStatusMessage = '';
      }
    );
  }

  deletePlayer(player) {
    console.log(`Admin has requested to delete player ${player.username}`);
    this.showSpinner = true;
    this.deleteModalStatusMessage = 'Submitting...';
    this.apiService.delete(`/players/${player.id}`).subscribe(
      resp => {
        console.log(`Player deleted!`);
        this.deleteModalStatusMessage = 'Deleted!';
        this.loadData();
        setTimeout(() => {
          this.deleteModalRef.close();
        }, 1000);
      },
      (err) => {
        this.deleteModalStatusMessage = `Error: ${err.message}`;
      },
      () => {
        this.showSpinner = false;
      }
    );
  }

  buildInviteForm() {
    const initialMessage = `I would like you to join my team.`;
    this.inviteForm = this.fb.group({
      emails: this.fb.array([
        new FormControl(null)
      ]),
      invite_message: [initialMessage, [
        Validators.required,
        Validators.minLength(12)
      ]]
    });
  }

  addInviteEmail() {
    const control = <FormArray>this.inviteForm.controls['emails'];
    control.push(new FormControl(null));
  }

  deleteInviteEmail(index: number) {
    const control = <FormArray>this.inviteForm.controls['emails'];
    control.removeAt(index);
  }

  submitInviteForm() {
    this.showSpinner = true;
    console.log(`Submitting invite form`);
    const emails = [];
    const controls = <FormArray>this.inviteForm.controls['emails'];
    controls.controls.forEach(
      (formControl: FormControl) => {
        emails.push(formControl.value);
      }
    );
    console.log(`Inviting: ${JSON.stringify(emails)}`);
    const payload = {
      'data': {
        'emails': emails,
        'message': this.inviteForm.controls['invite_message'].value,
        'team_id': this.currentTeam.id
      }
    };
    this.apiService.post('/players/invite', payload).subscribe(
      (resp) => {
        console.log(`Submitted to celery queue!`);
        this.inviteModalStatusMessage = 'Successfully submitted invites!';
      },
      (err) => {
        console.log(`Error submitted invite emails`);
        this.inviteModalStatusMessage = `Error submitting invites: ${err.message}`;
        this.showSpinner = false;
      },
      () => {
        this.showSpinner = false;
        this.loadData();
      }
    );
  }

}
