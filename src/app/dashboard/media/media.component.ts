import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Player } from '../../shared/models/player';
import { forkJoin } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '../../../../node_modules/@angular/forms';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'gamma-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent implements OnInit {

  currentPlayer: Player = new Player();
  images: any[] = [];
  sounds: any[] = [];

  showSpinner = false;
  formStatusMessage = '';
  imageForm: FormGroup;
  imageFileToUpload: File;
  soundForm: FormGroup;
  soundFileToUpload: File;

  constructor(
    private apiService: ApiService,
    private fb: FormBuilder,
    private modalSerivce: NgbModal
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.loadMedia();
    this.buildForms();
  }

  loadMedia() {
    this.images = [];
    this.sounds = [];
    forkJoin(
      this.apiService.get('/images'),
      this.apiService.get('/sounds')
    ).subscribe(([imageResp, soundResp]) => {
      this.images = imageResp.items;
      this.sounds = soundResp.items;
    },
    (err) => {},
    () => {
      console.log(`Finished grabbing image and sound resources`);
      // console.log(JSON.stringify(this.images, null, 2));
    }
  );
  }

  buildForms() {
    this.imageForm = this.fb.group({
      file: [null, [
        Validators.required
      ]]
    });

    this.soundForm = this.fb.group({
      file: [null, [
        Validators.required
      ]]
    });
  }

  open(modelTemplate) {
    this.modalSerivce.open(modelTemplate).result.then(
      (result) => {},
      (result) => {
        // make sure form is reset
        this.imageForm.reset();
        this.formStatusMessage = '';
        this.showSpinner = false;
        console.log(`Closed modal and reset form`);
      }
    );
  }

  submitImageForm() {
    if (this.imageForm.valid) {
      console.log(JSON.stringify(this.imageForm.value, null, 2));
      console.log(`Attempting to upload to S3`);
      this.showSpinner = true;
      this.formStatusMessage = 'Uploading...';

      const formData: FormData = new FormData();
      formData.append('file', this.imageFileToUpload, this.imageFileToUpload.name);
      console.log(JSON.stringify(formData));

      this.apiService.uploadFile(`/images`, formData).subscribe(
        resp => {
          console.log(resp);
          this.formStatusMessage = 'Uploaded!';
          this.loadMedia();
        },
        (err) => {
          console.log(`Error posting new image: ${JSON.stringify(err)}`);
          this.formStatusMessage = 'Error uploading new image';
        },
        () => {
          console.log(`Completed call to upload new image`);
          this.showSpinner = false;
        }
      );
    }
  }

  submitSoundForm() {
    if (this.imageForm.valid) {
      console.log(JSON.stringify(this.soundForm.value, null, 2));
      console.log(`Attempting to upload to S3`);
      this.showSpinner = true;
      this.formStatusMessage = 'Uploading...';

      const formData: FormData = new FormData();
      formData.append('file', this.soundFileToUpload, this.soundFileToUpload.name);
      console.log(JSON.stringify(formData));

      this.apiService.uploadFile(`/sounds`, formData).subscribe(
        resp => {
          console.log(resp);
          this.formStatusMessage = 'Uploaded!';
          this.loadMedia();
        },
        (err) => {
          console.log(`Error posting new sound: ${JSON.stringify(err)}`);
          this.formStatusMessage = 'Error uploading new sound';
        },
        () => {
          console.log(`Completed call to upload new sound`);
          this.showSpinner = false;
        }
      );
    }
  }

  handleImageFileInput(event) {
    const eventObj: MSInputMethodContext = <MSInputMethodContext> event;
    const target: HTMLInputElement = <HTMLInputElement> eventObj.target;
    const files: FileList = target.files;
    this.imageFileToUpload = files[0];
    console.log(this.imageFileToUpload);
  }

  handleSoundFileInput(event) {
    const eventObj: MSInputMethodContext = <MSInputMethodContext> event;
    const target: HTMLInputElement = <HTMLInputElement> eventObj.target;
    const files: FileList = target.files;
    this.soundFileToUpload = files[0];
    console.log(this.soundFileToUpload);
  }

}
