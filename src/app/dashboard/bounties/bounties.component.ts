import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Team } from '../../shared/models/team';
import { Player } from '../../shared/models/player';
import { forkJoin } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '../../../../node_modules/@angular/forms';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders } from '../../../../node_modules/@angular/common/http';

@Component({
  selector: 'gamma-bounties',
  templateUrl: './bounties.component.html',
  styleUrls: ['./bounties.component.css']
})
export class BountiesComponent implements OnInit {

  currentPlayer: Player = new Player();
  currentTeam: Team = new Team();
  bounties: any[] = [];

  httpOpts;

  bountyForm: FormGroup;
  gitlabForm: FormGroup;
  formStatusMessage = '';
  showSpinner = false;
  bountyModalSteps = [
    {
      name: 'Create bounty',
      complete: false,
      icon: 'clock',
      color: 'text-muted',
      error: ''
    },
    {
      name: 'Check the project for configured webhooks',
      complete: false,
      icon: 'clock',
      color: 'text-muted',
      error: ''
    },
    {
      name: 'Add bounty label to issue',
      complete: false,
      icon: 'clock',
      color: 'text-muted',
      error: ''
    },
    {
      name: 'Add comment to issue',
      complete: false,
      icon: 'clock',
      color: 'text-muted',
      error: ''
    }
  ];

  gitlabApiUrl = '';
  teamProjects: any[] = [];
  projectIssues: any[] = [];
  selectedIssue;

  constructor(
    private apiService: ApiService,
    private http: HttpClient,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.loadData();
    this.buildForms();
  }

  loadData() {
    this.teamProjects = [];
    this.projectIssues = [];
    forkJoin(
      this.apiService.get(`/teams/${this.currentPlayer.team_id}`),
      this.apiService.get(`/bounties`)
    ).subscribe(
      ([teamResp, bountyResp]) => {
        this.bounties = [];
        this.gitlabApiUrl = teamResp.gitlab_url + '/api/v4';
        this.currentTeam = teamResp;
        this.bounties = bountyResp.items;
      },
      (err) => {
        console.log(`Error loading bounty data: ${JSON.stringify(err)}`);
      },
      () => {
        const httpOpts = {
          headers: new HttpHeaders({
            'PRIVATE-TOKEN': this.currentTeam.gitlab_private_token
          })
        };
        // wait until the initial page data is loaded
        this.loadTeamProjects();
      }
    );
  }

  loadTeamProjects() {
    const httpOpts = {
      headers: new HttpHeaders({
        'PRIVATE-TOKEN': this.currentTeam.gitlab_private_token
      })
    };
    console.log(`Getting team projects...`);
    console.log(JSON.stringify(httpOpts));
    this.http.get(`${this.gitlabApiUrl}/projects?owned=true`, httpOpts).subscribe(
      (resp: any[]) => {
        console.log(`Iterating through projects`);
        resp.forEach(project => {
          console.log(`Found project ${project.name}`);
          this.teamProjects.push({
            name: project['name'],
            id: project['id']
          });
        });
      },
      (err) => {
        console.log(err);
      },
      () => {
        console.log(`Finished getting projects, found:`);
        console.log(JSON.stringify(this.teamProjects));
      }
    );
  }

  buildForms() {
    this.bountyForm = this.fb.group({
      project: [null, []],
      issue: [null, []],
      name: [null, [
        Validators.required
      ]],
      description: [null, [
        Validators.required
      ]],
      xp_value: [null, [
        Validators.required
      ]]
    });

    this.gitlabForm = this.fb.group({
      url: [null, []],
      hook: [null, []],
      private: [null, []],
      label: [null, []]
    });
  }

  get fName() { return this.bountyForm.get('name'); }
  get fProject() { return this.bountyForm.get('project'); }
  get fIssue() { return this.bountyForm.get('issue'); }
  get fDescription() { return this.bountyForm.get('description'); }
  get fXpValue() { return this.bountyForm.get('xp_value'); }

  get fUrl() { return this.gitlabForm.get('url'); }
  get fHook() { return this.gitlabForm.get('hook'); }
  get fPrivate() { return this.gitlabForm.get('private'); }
  get flabel() { return this.gitlabForm.get('label'); }



  open(template) {
    this.modalService.open(template).result.then(
      (result) => {},
      (result) => {
        // make sure form is reset
        this.bountyForm.reset();
        this.formStatusMessage = '';
        this.showSpinner = false;
        this.bountyModalSteps.forEach(step => {
          step.color = 'text-muted';
          step.icon = 'clock';
          step.complete = false;
          step.error = '';
        })
        console.log(`Closed modal and reset form`);
      }
    );
  }

  submitBountyForm() {
    // TODO maybe switch to concatMap:
    // https://stackoverflow.com/a/45815409 
    console.log(JSON.stringify(this.bountyForm.value));
    if ((this.bountyForm.valid) &&
        (this.bountyForm.dirty || this.bountyForm.touched)) {
      this.showSpinner = true;
      const payload = {
        'name': this.fName.value,
        'description': this.fDescription.value,
        'xp_reward': this.fXpValue.value,
        'issue_id': this.selectedIssue.iid,
        'issue_url': this.selectedIssue.web_url,
        'team_id': this.currentPlayer.team_id,
        'project': this.teamProjects.filter(p => +p.id === +this.fProject.value)[0]['name']
      };
      console.log(JSON.stringify(payload));
      this.apiService.post(`/bounties`, payload).subscribe(
        resp => {
          console.log(`Created new bounty!`);
          // this.loadData();
          this.formStatusMessage = 'New bounty has been created!';
          this.bountyModalSteps[0].complete = true;
          this.bountyModalSteps[0].icon = 'thumbs-up';
          this.bountyModalSteps[0].color = 'text-success';
        },
        (err) => {
          console.log(JSON.stringify(err));
          this.bountyModalSteps[0].icon = 'thumbs-down';
          this.bountyModalSteps[0].color = 'text-danger';
          this.bountyModalSteps[0].error = err.statusText;
        },
        () => {
          const httpOpts = {
            headers: new HttpHeaders({
              'PRIVATE-TOKEN': this.currentTeam.gitlab_private_token
            })
          };
          // after backend creation, make sure project has a webhook attached
          this.http.get(`${this.gitlabApiUrl}/projects/${this.fProject.value}/hooks`, httpOpts).subscribe(
            (resp: any[]) => {
              // endpoint returns a list, but only has one item
              const hooks = resp[0];
              if ((resp.length === 0) || (!hooks.issues_events || !hooks.note_events)) {
                console.log(`Webhooks missing from this project, adding`);
                const newHooks = {
                  'id': this.fProject.value,
                  'url': 'http://project-gamma-backend.herokuapp.com',
                  'issues_events': true,
                  'note_events': true,
                  'token': this.currentTeam.gitlab_hook_token,
                  'enable_ssl_verification': false
                };
                this.http.post(`${this.gitlabApiUrl}/projects/${this.fProject.value}/hooks`, newHooks, httpOpts).subscribe(
                  resp => {
                    console.log(`Finished adding webhooks!`);
                    console.log(JSON.stringify(resp));
                    this.bountyModalSteps[1].complete = true;
                    this.bountyModalSteps[1].icon = 'thumbs-up';
                    this.bountyModalSteps[1].color = 'text-success';
                  },
                  (err) => {
                    this.bountyModalSteps[1].icon = 'thumbs-down';
                    this.bountyModalSteps[1].color = 'text-danger';
                    this.bountyModalSteps[1].error = err.statusText;
                  }
                );
              } else {
                console.log(`Webhooks already attached to this project`);
                this.bountyModalSteps[1].complete = true;
                this.bountyModalSteps[1].icon = 'thumbs-up';
                this.bountyModalSteps[1].color = 'text-success';
              }
            },
            (err) => {
              this.bountyModalSteps[1].icon = 'thumbs-down';
              this.bountyModalSteps[1].color = 'text-danger';
              this.bountyModalSteps[1].error = err.statusText;
            },
            () => {
              console.log(`Moving to check labels`);
              let hasBountyLabel = false;
              const httpOpts = {
                headers: new HttpHeaders({
                  'PRIVATE-TOKEN': this.currentTeam.gitlab_private_token
                })
              };
              this.http.get(`${this.gitlabApiUrl}/projects/${this.fProject.value}/labels`, httpOpts).subscribe(
                (resp: any[]) => {
                  resp.forEach(label => {
                    if (label.name === this.currentTeam.gitlab_bounty_label) {
                      hasBountyLabel = true;
                    }
                  });
                  if (!hasBountyLabel) {
                    console.log(`Bounty label does not exist in project, creating`);
                    const newLabel = {
                      'id': this.fProject.value,
                      'name': this.currentTeam.gitlab_bounty_label,
                      'color': 'green'
                    };
                    this.http.post(`${this.gitlabApiUrl}/projects/${this.fProject.value}/labels`, newLabel, httpOpts).subscribe(
                      (resp: any[]) => {
                        console.log(`New bounty label has been created!`);
                        this.bountyModalSteps[2].complete = true;
                        this.bountyModalSteps[2].icon = 'thumbs-up';
                        this.bountyModalSteps[2].color = 'text-success';
                      },
                      (err) => {
                        this.bountyModalSteps[2].icon = 'thumbs-down';
                        this.bountyModalSteps[2].color = 'text-danger';
                        this.bountyModalSteps[2].error = JSON.stringify(err);
                      }
                    )
                  } else {
                    console.log(`Bounty label already exists, skipping`);
                    this.bountyModalSteps[2].complete = true;
                    this.bountyModalSteps[2].icon = 'thumbs-up';
                    this.bountyModalSteps[2].color = 'text-success';
                  }
                },
                (err) => {
                  this.bountyModalSteps[2].icon = 'thumbs-down';
                  this.bountyModalSteps[2].color = 'text-danger';
                  this.bountyModalSteps[2].error = err.statusText;
                },
                () => {
                  console.log(`Adding comment to issue`);
                  const newComment = {
                    'id': this.fProject.value,
                    'issue_id': this.selectedIssue.iid,
                    'body': `${this.currentPlayer.username} has created a bounty for this issue! It is worth ${this.fXpValue.value}XP.`
                  };
                  console.log(`Selected issue: ${JSON.stringify(this.selectedIssue)}`);
                  this.http.post(`${this.gitlabApiUrl}/projects/${this.fProject.value}/issues/${this.selectedIssue.iid}/notes`, newComment, httpOpts).subscribe(
                    resp => {
                      console.log(`New comment added to issue!`);
                      this.bountyModalSteps[3].complete = true;
                      this.bountyModalSteps[3].icon = 'thumbs-up';
                      this.bountyModalSteps[3].color = 'text-success';
                    },
                    (err: Response) => {
                      this.bountyModalSteps[3].icon = 'thumbs-down';
                      this.bountyModalSteps[3].color = 'text-danger';
                      this.bountyModalSteps[3].error = JSON.stringify(err.statusText);
                    },
                    () => {
                      this.formStatusMessage = 'All steps executed';
                      this.loadData();
                    }
                  )
                }
              )
            }
          );
          this.showSpinner = false;
        }
      );
    }
  }

  onProjectChange(id) {
    console.log(`onProjectChange() called with id ${id}`);
    console.log(JSON.stringify(this.teamProjects));
    this.showSpinner = true;
    this.formStatusMessage = 'Loading project issues..';
    this.projectIssues = [];
    this.selectedIssue = null;
    const httpOpts = {
      headers: new HttpHeaders({
        'PRIVATE-TOKEN': this.currentTeam.gitlab_private_token
      })
    };

    this.http.get(`${this.gitlabApiUrl}/projects/${id}/issues?state=opened`, httpOpts).subscribe(
      (resp: any[]) => {
        this.formStatusMessage = 'Grabbed issues for selected project!';
        resp.forEach(issue => {
          console.log(JSON.stringify(issue, null, 2));
          this.projectIssues.push(issue);
        });
        if (resp.length === 0) {
          this.formStatusMessage = 'No issues found for selected project';
        }
      },
      (err) => {
        console.log(JSON.stringify(err));
        this.formStatusMessage = 'Error getting issues for project';
      },
      () => {
        this.showSpinner = false;
      }
    );
  }

  onIssueChange(id) {
    const issue = this.projectIssues.filter(i => {
      return +i.id === +id;
    });
    this.selectedIssue = issue[0];
    console.log(JSON.stringify(issue));
  }

  submitGitlabForm() {
    if ((this.gitlabForm.valid) &&
        (this.gitlabForm.dirty || this.gitlabForm.touched)) {
          const payload = {
            'gitlab_url': this.fUrl.value,
            'gitlab_hook_token': this.fHook.value,
            'gitlab_private_token': this.fPrivate.value,
            'gitlab_bounty_label': this.flabel.value
          };
          this.apiService.put(`/teams/${this.currentTeam.id}`, payload).subscribe(
            resp => {
              console.log(JSON.stringify(resp));
              this.loadData();
            }
          );
        }
  }
}
