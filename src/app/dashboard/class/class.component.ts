import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { FormBuilder, FormGroup, Validators } from '../../../../node_modules/@angular/forms';
import { NgbModal } from '../../../../node_modules/@ng-bootstrap/ng-bootstrap';
import { Player } from '../../shared/models/player';
import { Team } from '../../shared/models/team';

@Component({
  selector: 'gamma-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit {

  currentPlayer: Player = new Player();

  classes: any[] = [];
  images: any[] = [];

  showSpinner = false;
  // create class modal
  createModalRef: any;
  createModalStatus = '';
  createModalForm: FormGroup;
  createModalImageId;

  // delete class modal
  deleteModalRef: any;
  deleteModalStatus = '';
  deleteModalClass: any;

  // edit class modal
  editModalRef: any;
  editModalStatus = '';
  editModalClass: any;
  editModalForm: FormGroup;
  editModalImageId;


  constructor(
    private apiService: ApiService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.buildForms();
    this.loadData();
  }

  loadData() {
    this.classes = [];
    this.apiService.get(`/classes`).subscribe(
      (resp) => {
        this.classes = resp.items;
      }
    );

    // load images for classes
    this.images = [];
    this.apiService.get('/images').subscribe(
      resp => {
        this.images = resp.items;
      }
    );
  }

  buildForms() {
    this.createModalForm = this.fb.group({
      name: [null, [
        Validators.required
      ]],
      description: [null, [
        Validators.required
      ]],
      level_requirement: [null, [
        Validators.min(2)
      ]],
      pic_id: [null, [
        Validators.required
      ]]
    });

    this.editModalForm = this.fb.group({
      name: [null, [
        Validators.required
      ]],
      description: [null, [
        Validators.required
      ]],
      level_requirement: [null, [
        Validators.min(2)
      ]],
      pic_id: [null, [
        Validators.required
      ]]
    });
  }

  get fCreateName() { return this.createModalForm.get('name'); }
  get fCreateDescription() { return this.createModalForm.get('description'); }
  get fCreateLevelRequirement() { return this.createModalForm.get('level_requirement'); }
  get fCreatePicId() { return this.createModalForm.get('pic_id'); }

  get fEditName() { return this.createModalForm.get('name'); }
  get fEditDescription() { return this.createModalForm.get('description'); }
  get fEditLevelRequirement() { return this.createModalForm.get('level_requirement'); }
  get fEditPicId() { return this.createModalForm.get('pic_id'); }

  openCreateModal(template) {
    this.createModalRef = this.modalService.open(template);
    this.createModalRef.result.then(
      (result) => {},
      (reason) => {
        console.log(`Dismissed modal dialog`);
        this.createModalStatus = '';
        this.createModalForm.reset();
        this.createModalImageId = null;
      }
    );
  }

  selectCreateImage(id) {
    this.createModalImageId = id;
  }

  createNewClass() {
    if (this.createModalImageId === null) {
      this.createModalStatus = 'Must select a picture!';
      return;
    }
    console.log(`Attempting to create a new class`);
    this.showSpinner = true;
    this.createModalStatus = 'Submitting..';

    const payload = {
      'name': this.fCreateName.value,
      'description': this.fCreateDescription.value,
      'level_requirement': this.fCreateLevelRequirement.value,
      'pic_id': this.createModalImageId,
      'team_id': this.currentPlayer.team_id
    };
    this.apiService.post(`/classes`, payload).subscribe(
      resp => {
        console.log(`Created!`);
        this.createModalStatus = 'Submitted!';
        this.loadData();
      },
      (err) => {
        this.createModalStatus = `Error: ${err.message}`;
      },
      () => {
        this.showSpinner = false;
        setTimeout(() => {
          this.createModalRef.close();
        }, 500);
      }
    );
  }

  openEditModal(player_class, template) {
    this.editModalClass = player_class;
    this.editModalRef = this.modalService.open(template);
    this.editModalRef.result.then(
      (result) => {},
      (reason) => {
        console.log(`Dismissed modal dialog`);
        this.editModalStatus = '';
        this.editModalClass = null;
        this.editModalImageId = null;
        this.editModalForm.reset();
      }
    );
  }

  selectEditImage(id) {
    this.editModalImageId = id;
  }

  submitEditClass() {}

  openDeleteModal(player_class, template) {
    this.deleteModalClass = player_class;
    this.deleteModalRef = this.modalService.open(template, {size: 'sm'});
    this.deleteModalRef.result.then(
      (result) => {},
      (reason) => {
        console.log(`Dismissed modal dialog`);
        this.deleteModalStatus = '';
        this.deleteModalClass = null;
      }
    );
  }

  submitDeleteClass() {
    console.log(`Admin has requested to delete player ${this.deleteModalClass.name}`);
    this.showSpinner = true;
    this.deleteModalStatus = 'Submitting...';
    this.apiService.delete(`/classes/${this.deleteModalClass.id}`).subscribe(
      resp => {
        console.log(`Class deleted!`);
        this.deleteModalStatus = 'Deleted!';
        this.loadData();
        setTimeout(() => {
          this.deleteModalRef.close();
        }, 1000);
        this.deleteModalStatus = '';
        this.deleteModalClass = null;
      },
      (err) => {
        this.deleteModalStatus = `Error: ${err.message}`;
      },
      () => {
        this.showSpinner = false;
      }
    );
  }
}
