import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { SortablejsModule } from 'angular-sortablejs';
import { MomentModule } from 'ngx-moment';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { ApiService } from './shared/services/api.service';
import { JwtService } from './shared/services/jwt.service';
import { DashboardModule } from './dashboard/dashboard.module';
import { SharedModule } from './shared/shared.module';
import { FeatherIconsPipe } from './shared/feather-pipe';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { Router, UrlSerializer } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { AuthGuard } from './shared/auth.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminGuard } from './shared/admin.guard';
import { CleanUrlSerializer } from './shared/url.serializer';
import { TeamComponent } from './team/team.component';
import { NewTeamComponent } from './login/newteam.component';
import { NewplayerComponent } from './login/newplayer.component';
import { ProfileComponent } from './profile/profile.component';
import { MobComponent } from './mob/mob.component';
import { InviteComponent } from './invite/invite.component';
import { MarketplaceComponent } from './marketplace/marketplace.component';
import { BountiesComponent } from './bounties/bounties.component';
import { RefreshTokenInterceptor } from './shared/token-interceptor';
import { ResetComponent } from './login/reset.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    TeamComponent,
    NewTeamComponent,
    NewplayerComponent,
    ProfileComponent,
    MobComponent,
    InviteComponent,
    MarketplaceComponent,
    BountiesComponent,
    ResetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    DashboardModule,
    NgbModule.forRoot(),
    SharedModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    LoadingBarHttpClientModule,
    SortablejsModule.forRoot({}),
    MomentModule,
    ToastrModule.forRoot()
  ],
  providers: [
    ApiService,
    JwtService,
    FeatherIconsPipe,
    AuthGuard,
    AdminGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenInterceptor,
      multi: true
    }
    // {
    //   provide: UrlSerializer,
    //   useClass: CleanUrlSerializer
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private router: Router) {
    // console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  }
}
