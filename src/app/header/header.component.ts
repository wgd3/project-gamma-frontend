import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Player } from '../shared/models/player';

@Component({
  selector: 'gamma-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  currentPlayer: Player;
  isLoggedIn: boolean;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (userData) => {
        this.currentPlayer = userData;
      }
    );
    this.apiService.isAuthenticated.subscribe(
      (isAuthenticated) => {
        this.isLoggedIn = isAuthenticated;
      }
    );
  }

  logout() {
    this.apiService.purgeAuth();
    console.log(`Logged user out...`);
    window.location.replace('/');
  }

  get profile_alert_count(): number {
    let count = 0;
    if (this.currentPlayer.has_temp_password === true) {
        count += 1;
    }
    if ((this.currentPlayer.first_name === null) ||
        (this.currentPlayer.last_name === null)) {
            count += 1;
    }
    return count;
  }

  get profile_alert_list(): any[] {
    const alerts = [];
    if (this.currentPlayer.has_temp_password === true) {
        alerts.push('Need to update your password');
    }
    if ((this.currentPlayer.first_name === null) ||
        (this.currentPlayer.last_name === null)) {
        alerts.push('Need to add your first/last name');
    }
    return alerts;
  }
}
