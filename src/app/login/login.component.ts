import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../shared/services/api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'gamma-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  passResetModalRef: any;
  resetForm: FormGroup;
  showSpinner = false;
  resetPassFormStatusMessage = '';

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private modalService: NgbModal,
    private router?: Router
  ) {  }

  ngOnInit() {
    this.buildLoginForm();
    this.buildResetForm();
  }

  private buildLoginForm() {
    this.form = this.fb.group({
      email: ['', [
        Validators.required
      ]
      ],
      password: ['', [
        Validators.required,
        Validators.minLength(8)
      ]
      ]
    });
  }

  private buildResetForm() {
    this.resetForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]]
    });
  }
  get fResetEmail() { return this.resetForm.get('email'); }

  get fEmail() { return this.form.get('email'); }
  get fPassword() { return this.form.get('password'); }

  login(redirect: boolean = true) {
    console.log(`Attempting log in...`);
    const val = this.form.value;
    // console.log(JSON.stringify(val));

    if (val.email && val.password) {
      const credentials = {
        email: val.email,
        password: val.password
      };
      this.apiService.login(credentials)
        .subscribe(
          (resp) => {
            console.log(`login resp: ${resp}`);
            console.log('User has logged in');
            // this.router.navigate(['/']);
            if (redirect) {
              window.location.replace('/');
            }
          },
          (err) => {
            console.log(`Error during login: ${JSON.stringify(err, null, 2)}`);
            if (err.status === 400) {
              console.log(`400 - bad username or password`);
              this.fEmail.setErrors({ invalid: true});
              this.fPassword.setErrors({ invalid: true});
            }
          }
        );
    } else {
      console.log(`Need both email and password`);
    }
  }

  openPasswordResetModal(modalTemplate) {
    this.passResetModalRef = this.modalService.open(modalTemplate);
  }

  closeModal() {
    this.passResetModalRef.result.then((result) => {
    }, (reason) => {
      console.log('Closing modal and resetting form');
      this.resetForm.reset();
      this.showSpinner = false;
      this.resetPassFormStatusMessage = '';
    });
  }

  resetPassword() {
    if (this.fResetEmail.valid) {
      this.showSpinner = true;
      const payload = {
        'email': this.fResetEmail.value
      };
      this.apiService.post(`/resetpassword`, payload).subscribe(
        resp => {
          console.log(`Successfully sent password reset email`);
        },
        (err) => {
          this.showSpinner = false;
          this.resetPassFormStatusMessage = 'Check your inbox!';
        },
        () => {
          this.showSpinner = false;
          this.resetPassFormStatusMessage = 'Check your inbox!';
        }
      );
    } else {
      this.resetPassFormStatusMessage = 'Please enter a valid email';
    }
  }

}
