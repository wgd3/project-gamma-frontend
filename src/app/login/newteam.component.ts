import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../shared/services/api.service';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'gamma-newteam',
  templateUrl: './newteam.component.html',
  styleUrls: ['./newteam.component.css']
})
export class NewTeamComponent implements OnInit {

  busy = false;
  teamForm: FormGroup = new FormGroup({});
  teamFormSubmitted = false;
  newTeamId: number;
  adminUserForm: FormGroup = new FormGroup({});

  teamFormSubmitBtn = {
    text: 'Submit',
    class: 'btn-primary',
    disabled: false
  };
  teamFormSubmitStatus = {
    class: '',
    text: ''
  };
  adminFormSubmitBtn = {
    text: 'Submit',
    class: 'btn-primary',
    disabled: false
  };
  adminFormSubmitStatus = {
    class: '',
    text: ''
  };

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.buildForms();
  }

  buildForms() {
    this.teamForm = this.fb.group({
      team_name: ['', Validators.required],
      def_interval_length: [7, Validators.required]
    });

    this.adminUserForm = this.fb.group({
      username: ['', [
        Validators.required,
        Validators.minLength(6)
        ]
      ],
      email: ['', [
        Validators.required,
        Validators.pattern('[^ @]*@[^ @]*')
        ]
      ],
      password: ['', [
        Validators.required,
        Validators.minLength(8)
        ]
      ],
      matching_password: ['', [
        Validators.required,
        Validators.minLength(8)
        ]
      ]
    });

  }

  get team_name() { return this.teamForm.get('team_name'); }
  get def_interval_length() { return this.teamForm.get('def_interval_length'); }

  get username() { return this.adminUserForm.get('username'); }
  get email() { return this.adminUserForm.get('email'); }
  get password() { return this.adminUserForm.get('password'); }
  get matching_password() { return this.adminUserForm.get('matching_password'); }

  submitNewTeam() {
    if (this.teamForm.valid) {
      const team_name = this.team_name.value;
      const interval = this.def_interval_length.value;
      const payload = {
        'data': {
          'name': team_name,
          'default_interval_length': interval
        }
      };
      this.busy = true;
      this.apiService.post('/teams', payload).subscribe(
        (resp) => {
          this.teamFormSubmitBtn.text = 'Created!';
          this.teamFormSubmitBtn.class = 'btn-success';
          this.teamFormSubmitBtn.disabled = true;

          this.teamFormSubmitStatus.text = 'Team created!';
          this.teamFormSubmitStatus.class = 'text-success';
          this.teamFormSubmitted = true;
          this.newTeamId = resp['id'];
          console.log(`New team ${team_name} created successfully with ID ${this.newTeamId}!`);
        },
        (err) => {
          console.log(`Error creating team: ${JSON.stringify(err)}`);
          this.teamFormSubmitBtn.class = 'btn-warning';
          this.teamFormSubmitStatus.text = `${err.error.message} - Please try again.`;
          this.teamFormSubmitStatus.class = 'text-danger';
        },
        () => {
          this.busy = false;
          console.log(`Finished attempting to submit new team`);
        }
      );
    }

  }

  submitTeamAdmin() {
    if (this.adminUserForm.valid) {
      this.busy = true;
      this.apiService.post('/players', {
        'data': {
          'username': this.username.value,
          'email': this.email.value,
          'password': this.password.value,
          'team_id': this.newTeamId,
          'team_admin': true
        }
      }).subscribe(
        (resp) => {
          this.adminFormSubmitBtn.text = 'Created!';
          this.adminFormSubmitBtn.class = 'btn-success';
          this.adminFormSubmitBtn.disabled = true;

          this.adminFormSubmitStatus.text = 'Admin user created! You will be redirected to log in momentarily.';
          this.adminFormSubmitStatus.class = 'text-success';
          console.log(`Created new user with id ${resp['id']}`);

          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 2000);
        },
        (err) => {
          console.log(`Error creating player: ${JSON.stringify(err)}`);
          this.adminFormSubmitBtn.class = 'btn-warning';

          this.adminFormSubmitStatus.text = `${err.error.message} - Please try again.`;
          this.adminFormSubmitStatus.class = 'text-danger';
        },
        () => {
          this.busy = false;
          console.log(`finished creating new player`);
        }
      );
    }
  }
}
