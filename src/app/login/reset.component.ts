import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../shared/services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'gamma-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit, OnDestroy {

  slug: string;
  private sub: any;

  valid_slug: boolean;

  passwordForm: FormGroup;
  showSpinner: boolean;
  passwordFormStatusMessage: string;

  redirect_seconds = 5;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    console.log(`Detected password reset link, checking...`);
    this.sub = this.route.params.subscribe(params => {
      this.slug = params['slug'];
      this.checkResetSlug();
    });
    this.buildForm();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  checkResetSlug() {
    console.log(`Checking password reset token...`);
    this.apiService.get(`/resetpassword/${this.slug}`).subscribe(
      resp => {
        console.log('Valid token!');
        this.valid_slug = true;
      },
      (err) => {
        console.log(`error checking token: ${JSON.stringify(err)}`);
        this.valid_slug = false;
        setTimeout(() => {
          window.location.replace('/login');
        }, (this.redirect_seconds * 1000));
      }
    );
  }

  buildForm() {
    this.passwordForm = this.fb.group({
      new_password: [null, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50),
      ]],
      matching_password: [null, [
        Validators.required
      ]]
    });
  }

  get fNewPassword() { return this.passwordForm.get('new_password'); }
  get fMatchingPassword() { return this.passwordForm.get('matching_password'); }

  submitPasswordForm() {
    if (this.passwordForm.valid === true) {
      if (this.fNewPassword.value === this.fMatchingPassword.value) {
        this.showSpinner = true;
        console.log(`Updating player password..`);
        this.apiService.put(`/resetpassword`, {
          'new_password': this.fNewPassword.value,
          'token': this.slug
        }).subscribe(
          resp => {
            console.log(`Updated password!`);
            this.passwordFormStatusMessage = `Updated your password! Redirecting in ${this.redirect_seconds}`;
            setTimeout(() => {
              window.location.replace('/login');
            }, (this.redirect_seconds * 1000));
          },
          (err) => {
            console.log(`Error updating password: ${JSON.stringify(err)}`);
            this.passwordFormStatusMessage = 'Error updating password';
          },
          () => {
            this.showSpinner = false;
          }
        );
      } else {
        console.log(`Passwords do not match`);
        this.fNewPassword.setErrors({ 'notMatching': true });
        this.fMatchingPassword.setErrors({ 'notMatching': true });
        this.passwordFormStatusMessage = 'New passwords must match!';
      }
    } else {
      console.log(`Password form is not valid`);
    }
  }
}
