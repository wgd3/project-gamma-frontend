import { Component, OnInit, ViewChildren, QueryList, Output } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Team } from '../shared/models/team';
import { forkJoin } from 'rxjs';
import { Player } from '../shared/models/player';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl, AbstractControl } from '../../../node_modules/@angular/forms';
import { PlayerCardComponent } from '../shared/player-card/player-card.component';
import { NgbModal } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';
import { EventEmitter } from 'events';
import { MessageService } from '../shared/services/message.service';
import { Message } from '../shared/models/message';

export interface IMobTab {
  id: number; // unique tab id (equals mob ID)
  title: string; // tab text
}

export function ValidatePlayerCount(control: AbstractControl) {
  if (control.value.length >= 2) {
    return { validCount: true };
  }
  return null;
}

@Component({
  selector: 'gamma-mob',
  templateUrl: './mob.component.html',
  styleUrls: ['./mob.component.css']
})
export class MobComponent implements OnInit {

  // currentRef variables
  currentPlayer: Player = new Player();
  currentTeam: Team = new Team();

  messages: Message[] = [];

  // mob variables
  mobs: any[] = [];
  activeMob: any;
  sortableQueueOptions = {
    onUpdate: (event: any) => {
      console.log(`Sortable event: ${JSON.stringify(event.item)}`);
    },
    animation: 300,
    draggable: 'gamma-player-card'
  };
  @ViewChildren(PlayerCardComponent) playerQueue: QueryList<PlayerCardComponent>;
  alert_sounds = [];
  get alertSoundName() { 
    const sound = this.alert_sounds.filter(x => x.id === this.activeMob.sound_id)[0];
    // console.log(`Found alert sound ${sound.shortname} for mob ${this.activeMob.id}`);
    return sound.shortname;
  }
  availablePlayers: Player[] = [];

  // tab variables
  tabs: IMobTab[] = [];
  activeTabId: number;
  createNewMobTabId: number;

  // form variables
  newMobForm: FormGroup;
  editMobForm: FormGroup;
  editMobModelRef: any;
  editMobSuccess = false;

  // timer variables
  mob_timer;
  intervalId = 0; // not mob-interval, just for frontend timer tracking
  timer_seconds = 0;
  display_minutes = '00';
  display_seconds = '00';
  timer_color = '#00ff00';

  constructor(
    private apiService: ApiService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private logService: MessageService
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.logService.messages.subscribe(
      (resp: Message[]) => {
        this.messages = resp;
        this.messages.sort((a, b) => {
          if (moment(a.created).isBefore(b.created)) {
            return 1;
          }
          if (moment(a.created).isAfter(b.created)) {
            return -1;
          }
          return 0;
        });
        this.messages = this.messages.slice(0, 10);
      }
    );
    this.loadData();
    this.buildNewMobForm();
    this.buildModalEditForm();
  }

  loadData() {
    // start by getting current team
    this.apiService.get(`/teams/${this.currentPlayer.team_id}`).subscribe(
      (team: Team) => {
        this.currentTeam = team;
        this.availablePlayers = team.players.filter(p => p.in_mob === false);
      },
      (err) => {},
      () => {
        // reset mobs and tabs in case this is called outside of onInit
        this.mobs = [];
        this.tabs = [];

        // on completion, get all mobs
        this.apiService.get('/mobs?active=1').subscribe(
          resp => {
            // get all active mobs
            resp.items.forEach(
              mob => {
                if (mob.active) {
                  this.mobs.push(mob);

                  // create a tab for each active mob
                  this.addNewMobToPage(mob);
                }
              }
            );

            // add tab for creating mobs (mob id will always be > 0)
            this.createNewMobTabId = 0;
            this.tabs.push({
              id: this.createNewMobTabId,
              title: 'Create A New Mob'
            });

            // set 1st mob to active
            this.activeTabId = this.tabs[0].id;
            this.switchTab(this.activeTabId);

            // console.log(`Found ${this.mobs.length} mobs associated with team ${this.currentTeam.id}`);
          }
        );
      }
    );

    this.apiService.get('/sounds').subscribe(
      resp => {
        this.alert_sounds = [];
        resp.items.forEach((sound) => {
          sound.shortname = sound.path.split('/').pop();
          this.alert_sounds.push(sound);
        });
      }
    );
  }

  private addNewMobToPage(mob: any) {
    console.log(`Adding mob: ${JSON.stringify(mob)}`);
    const newTab = {
      id: mob.id,
      title: `${mob.topic}`
    };
    this.tabs.push(newTab);
  }

  switchTab(id: number) {
    console.log(`Switching to mob ${id}`);
    const targetTab = this._getMobTab(id);
    this.activeTabId = targetTab.id;
    if (id !== 0) {
      // only run this code when not selecting the Create New tab
      // this.activeMob = this._getMob(id);
      this.apiService.get(`/mobs/${id}`).subscribe(
        (resp) => {
          this.activeMob = resp;
          // make sure tab title is up to date
          const mobTab = this.tabs.filter(x => x.id === this.activeMob.id)[0];
          mobTab.title = `${this.activeMob.topic}`;
        },
        (err) => {},
        () => {
          this.timer_seconds = this.activeMob.interval_length * 60;
          this.clearTimer();
          this.updateTimerDisplay();
          this.checkForRunningInterval();
          // this.logService.loadRecentActivity();
        }
      );
    } else if (id === 0) {
      // reload team to make sure player availablility is up to date
      this.apiService.get(`/teams/${this.currentPlayer.team_id}`).subscribe(
        (team: Team) => {
          this.currentTeam = team;
        },
        (err) => {},
        () => {}
      );
    }
    // console.log(`activeMob players: ${JSON.stringify(this.activeMob, null, 2)}`);

    console.log(`Current tabs: ${JSON.stringify(this.tabs)}`);
    // preventDefault
    return false;
  }

  checkForRunningInterval() {
    console.log(`Calculating time remaining for mob ${this.activeMob.id}`);
    if (this.activeMob.current_interval_id !== null) {
      console.log(`Mob has active interval, getting details`);
      this.apiService.get(`/intervals/${this.activeMob.current_interval_id}`).subscribe(
        (resp) => {
          console.log(`GET interval resp: ${JSON.stringify(resp)}`);
          this.initializeTimer(resp);
        },
        (err) => {},
        () => {
          console.log(`Finished getting interval details`);
        }
      );
    } else {
      console.log(`Mob ${this.activeMob.id} does not have an interval currently active, not activating timer`);
    }
  }

  initializeTimer(interval_data: any) {
    // initially set interval length to mob default
    let time_remaining = this.activeMob.interval_length * 60;

    const start_time = moment.utc(interval_data.created);
    console.log('Timer started at ' + moment(start_time).toString());
    const current_time = moment.utc();
    console.log(`Momentjs thinks it is currently ${moment(current_time).toString()}`);
    const end_time = start_time.add(interval_data.duration, 'minutes');
    console.log('Timer expected to end at ' + moment(end_time).toString());

    time_remaining = end_time.diff(current_time, 'seconds');
    console.log(`Timer has ${time_remaining} seconds remaining`);
    this.timer_seconds = time_remaining;
    if (time_remaining <= 0) {
      console.log('Timer has expired, resetting...');
      this.stopMobTimer();
    }
    this.countDown();
  }

  private updateTimerDisplay() {
    const mins = Math.floor(this.timer_seconds / 60);
    let newDisplayMins = mins.toString();
    if (mins < 10) {
      newDisplayMins = '0' + mins;
    }
    const secs = this.timer_seconds - (mins * 60);
    let newDisplaySecs = secs.toString();
    if (secs < 10) {
      newDisplaySecs = '0' + secs;
    }
    this.display_minutes = newDisplayMins;
    this.display_seconds = newDisplaySecs;
    this.updateTimerColor();
  }

  private updateTimerColor() {
    let maxSec = (this.activeMob.interval_length * 60);
    let maxRGB = 254;
    let rgbRed = 126;
    let rgbGreen = 254;
    let rgbStr = '';
    if ( this.timer_seconds >= (maxSec / 2) ) {
        // increase red level for first half to transition to yellow
        let progress = ( (this.timer_seconds - (maxSec/2)) / (maxSec/2) ); // ie: (750-450)/450 = .666
        // console.log("progress: " + progress);
        let newRed = rgbRed + Math.floor((1-progress) * 126);
        rgbStr = 'rgb('+newRed+','+rgbGreen+', 0)';
        // console.log("Increasing red: " + rgbStr);
    } else {
        // decrease green to transition to red
        let progress = (this.timer_seconds / (maxSec / 2)); // ie: 350/450 = .777
        let newGreen = rgbGreen - Math.floor((1-progress) * maxRGB )
        rgbStr = 'rgb('+ maxRGB+','+newGreen+', 0)';
        // console.log("Decreasing green: " + rgbStr);
    }
    // var secProgress = (sec / maxSec);
    // var greenLvl = Math.floor(secProgress * 255);
    // var redLvl = Math.floor( (1-secProgress) * 255);
    // console.log("Setting timer background to " + rgbStr);
    this.timer_color = rgbStr;
  }

  clearTimer() { clearInterval(this.intervalId); }

  private countDown() {
    this.clearTimer();
    this.intervalId = window.setInterval(() => {
      this.timer_seconds -= 1;
      this.updateTimerDisplay();
      if (this.timer_seconds === 0) {
        console.log('Countdown complete!');
        this.clearTimer();
        this.ringAlert();
        // Adding 2 sec pause before stopping to make sure the last minute
        // is accounted for
        setTimeout(() => {
          this.stopMobTimer();
        }, 2000);
      }
    }, 1000);
  }


  private _getMobTab(id: number) {
    const tabs: IMobTab[] = this.tabs.filter(tab => tab.id === id);
    return tabs.length ? tabs[0] : null;
  }

  buildNewMobForm() {
    this.newMobForm = this.fb.group({
      topic: ['', [
        Validators.required,
        Validators.minLength(4)
        ]
      ],
      location: ['', [
        Validators.required,
        Validators.minLength(4)
        ]
      ],
      interval: ['', [
        Validators.required,
        ]
      ],
      players: this.fb.array([])
    });
  }

  buildModalEditForm() {
    this.editMobForm = this.fb.group({
      topic: ['', []],
      location: ['', []],
      interval: ['', []],
      sound: ['', []]
    });
  }

  submitMobEditForm() {
    console.log(`Submitting mob edit form`);
    const sound = this.alert_sounds.filter(x => x.shortname === this.fEditSound.value)[0];
    const payload = {};
    if (this.fEditInterval.dirty) {
      payload['interval_length'] = this.fEditInterval.value;
    }
    if (this.fEditSound.dirty) {
      payload['sound_id'] = sound.id;
    }
    if (this.fEditTopic.dirty) {
      payload['topic'] = this.fEditTopic.value;
    }
    if (this.fEditLocation.dirty) {
      payload['loaction'] = this.fEditLocation.value;
    }
    if (payload !== {}) {
      console.log(`Mob edit payload: ${JSON.stringify(payload, null, 2)}`);
      this.apiService.put(`/mobs/${this.activeMob.id}`, payload).subscribe(
        resp => {
          console.log(`edit mob resp: ${JSON.stringify(resp)}`);
          this.editMobSuccess = true;
          this.switchTab(this.activeMob.id);
          setTimeout(() => {
            this.editMobModelRef.close();
          }, 500);
        }
      );
    }
  }

  get fEditTopic() { return this.editMobForm.get('topic'); }
  get fEditLocation() { return this.editMobForm.get('location'); }
  get fEditInterval() { return this.editMobForm.get('interval'); }
  get fEditSound() { return this.editMobForm.get('sound'); }

  get fTopic() { return this.newMobForm.get('topic'); }
  get fLocation() { return this.newMobForm.get('location'); }
  get fInterval() { return this.newMobForm.get('interval'); }
  get fPlayers() { return this.newMobForm.get('players'); }

  playerChange(player_id: number, isChecked: boolean) {
    const playerFormArray = <FormArray>this.newMobForm.controls.players;

    if (isChecked) {
      playerFormArray.push(new FormControl(player_id));
    } else {
      const idx = playerFormArray.controls.findIndex(x => x.value === player_id);
      playerFormArray.removeAt(idx);
    }

    console.log(`${this.fPlayers.value.length} players are selected`);
    this.fPlayers.value.forEach(x => console.log(`Player ${x} is checked`));
  }

  submitNewMobForm() {
    console.log('Creating new mob...');
    // if (this.newMobForm.valid &&
    //     (this.newMobForm.dirty || this.newMobForm.touched)) {
      if (this.newMobForm.valid) {
        if (this.fPlayers.value.length < 2) {
          console.log(`Must select at least 2 players!`);
          return false;
        }
        const payload = {
          'topic': this.fTopic.value,
          'location': this.fLocation.value,
          'interval_length': this.fInterval.value,
          'team_id': this.currentTeam.id,
          'players': []
        };
        this.fPlayers.value.forEach(x => payload.players.push(x));
        this.apiService.post('/mobs', payload).subscribe(
          (resp) => {
            console.log(`new mob POST resp: ${JSON.stringify(resp)}`);
            this.addNewMobToPage(resp);
          },
          (err) => {
            console.log(`Error creating new mob: ${JSON.stringify(err)}`);
          },
          () => {
            console.log(`Finished submitting new mob!`);
            this.loadData();
          }
        );
    }
  }

  openEditModal(modalTemplate) {
    this.editMobModelRef = this.modalService.open(modalTemplate);
  }

  startMobTimer() {
    console.log(`Starting timer for mob ${this.activeMob.id}`);
    this.apiService.get(`/mobs/${this.activeMob.id}/start`).subscribe(
      (resp) => {
        console.log(`Start timer resp: ${JSON.stringify(resp)}`);
        this.activeMob.current_interval_id = resp.id;
        this.countDown();
      },
      (err) => {},
      () => {
        console.log(`Finished starting timer request`);
      }
    );
  }

  stopMobTimer() {
    console.log(`Stopping timer for mob ${this.activeMob.id}`);
    this.apiService.get(`/mobs/${this.activeMob.id}/stop`).subscribe(
      (resp) => {
        console.log(`Stop timer resp: ${JSON.stringify(resp)}`);
        this.activeMob.current_interval_id = null;
        // this.clearTimer();
        // this.timer_seconds = this.activeMob.interval_length * 60;
        // this.updateTimerDisplay();
        // this.switchTab(this.activeMob.id);
        // this.logService.loadRecentActivity();
      },
      (err) => {
        console.log(`Call to stop timer in backend failed: ${JSON.stringify(err)}`);
      },
      () => {
        console.log(`Finished stop timer request`);
        this.clearTimer();
        this.timer_seconds = this.activeMob.interval_length * 60;
        this.updateTimerDisplay();
        this.switchTab(this.activeMob.id);
        this.logService.loadRecentActivity();
      }
    );
  }

  disbandMob() {
    console.log(`Disbanding mob ${this.activeMob.id}`);
    this.apiService.get(`/mobs/${this.activeMob.id}/disband`).subscribe(
      (resp) => {
        console.log(`Disbanded!`);
        this.loadData();
      },
      (err) => {},
      () => {}
    );
  }

  ringAlert() {
    const player = <HTMLAudioElement>document.getElementById('alert-player');
    console.log(`Playing alert tone...`);
    player.play();
  }

  dropPlayerFromMob(player_id: number) {
    console.log(`Attempting to drop player ${player_id} from mob`);
    this.apiService.get(`/mobs/${this.activeMob.id}/drop/${player_id}`).subscribe(
      resp => {
        console.log(`Dropped player resp: ${JSON.stringify(resp)}`);
        this.switchTab(this.activeMob.id);
      },
      (err) => {},
      () => {
        this.reloadAvailablePlayers();
      }
    );
  }

  addPlayerToMob(player_id: number) {
    console.log(`Attempting to add player ${player_id} to mob`);
    this.apiService.get(`/mobs/${this.activeMob.id}/add/${player_id}`).subscribe(
      resp => {
        console.log(`Added player resp: ${JSON.stringify(resp)}`);
        this.switchTab(this.activeMob.id);
      },
      (err) => {},
      () => {
        this.reloadAvailablePlayers();
      }
    );
  }

  /**
   * Used when players on the team are added/dropped from the mob
   */
  reloadAvailablePlayers() {
    this.apiService.get(`/teams/${this.currentPlayer.team_id}`).subscribe(
      (team: Team) => {
        this.availablePlayers = team.players.filter(p => p.in_mob === false);
      }
    );
  }
}
