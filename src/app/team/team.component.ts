import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Team } from '../shared/models/team';
import { forkJoin } from 'rxjs';
import { Player } from '../shared/models/player';

@Component({
  selector: 'gamma-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  currentTeam: Team = new Team();
  currentPlayer: Player = new Player();
  teamPlayers: Player[] = [];
  cardStyle = 'default';

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        console.log(`Found player ${player.username}`);
        this.currentPlayer = player;
      }
    );
    this.loadData();
  }

  loadData() {
    forkJoin(
      this.apiService.get('/players'),
      this.apiService.get(`/teams/${this.currentPlayer.team_id}`)
    ).subscribe(
      ([playerRes, teamRes]) => {
        this.currentTeam = teamRes;
        this.teamPlayers = [];
        this.teamPlayers = playerRes.items;
      },
      (err) => {},
      () => {
        console.log(`Finished loading page data for team ${this.currentTeam.id}`);
      }
    );
  }

  switchCardSize(size: string) {
    this.cardStyle = size;
  }

}
