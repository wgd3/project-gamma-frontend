import { Injectable } from '@angular/core';
import { Message } from '../models/message';
import { BehaviorSubject, Observable } from '../../../../node_modules/rxjs';
import { ApiService } from './api.service';

@Injectable()
export class MessageService {
  // https://blog.angular-university.io/how-to-build-angular2-apps-using-rxjs-observable-data-services-pitfalls-to-avoid/

  private _messages: BehaviorSubject<Message[]> = <BehaviorSubject<Message[]>>new BehaviorSubject([]);
  public readonly messages: Observable<Array<Message>> = this._messages.asObservable();

  private readonly apiUrl = '/activity';

  constructor(
    private apiService: ApiService
  ) {
    this.loadActivity();
  }

  /**
   * This method grabs all activity logs the most recent 50 activity records
   */
  loadActivity() {
    this.apiService.get(`${this.apiUrl}?per_page=50&page=1`).subscribe(
      resp => {
        const messages = this._messages.getValue();
        resp.items.forEach(log => {
          messages.push(log);
        });
        this._messages.next(messages);
      }
    );
  }

  /**
   * This message grabs the most recent 25 messages from the current team.
   * There's no way (as of now) to tell the backend "new since last refresh",
   * so we'll parse through the response and make sure the activity's ID
   * (db primary key) doesn't exist in the current messages array.
   *
   * This method should be called when an action takes place that would log
   * activity, so the visual log can reflect the activity
   */
  loadRecentActivity() {
    this.apiService.get(`${this.apiUrl}?per_page=25&page=1`).subscribe(
      resp => {
        const messages = this._messages.getValue();
        resp.items.forEach(log => {
          if (!messages.includes(log)) {
            messages.push(log);
          }
        });
        this._messages.next(messages);
      },
      (err) => {},
      () => {
        console.log(`Refreshed activity log`);
      }
    );
  }

  addMessage(newMessage: Message): Observable<any> {
    const obs = this.apiService.post(this.apiUrl, JSON.stringify(newMessage));

    obs.subscribe(
      resp => {
        const newArr = this._messages.getValue();
        newArr.push(newMessage);
        this._messages.next(newArr);
      }
    );

    return obs;
  }

}
