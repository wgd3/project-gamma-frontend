import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class JwtService {

  constructor(
      private toastr: ToastrService
  ) { }

  static ACCESS_TOKEN_KEY = 'access_token';
  static REFRESH_TOKEN_KEY = 'refresh_token';

  getAccessToken(): string {
      return localStorage.getItem(JwtService.ACCESS_TOKEN_KEY);
  }

  getRefreshToken(): string {
      return localStorage.getItem(JwtService.REFRESH_TOKEN_KEY);
  }

  saveAccessToken(token: string) {
      localStorage.setItem(JwtService.ACCESS_TOKEN_KEY, token);
  }

  saveRefreshToken(token: string) {
      localStorage.setItem(JwtService.REFRESH_TOKEN_KEY, token);
  }

  destroyTokens() {
      localStorage.removeItem(JwtService.ACCESS_TOKEN_KEY);
      localStorage.removeItem(JwtService.REFRESH_TOKEN_KEY);
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) return null;

    const date = new Date(0); 
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    console.log(`JwtService: Checking token expiry`);
    if(!token) token = this.getAccessToken();
    if(!token) return true;

    const date = this.getTokenExpirationDate(token);
    if(date === undefined) { console.log(`No token date, marking expired`); return false; }
    const res = !(date.valueOf() > new Date().valueOf());
    console.log(`Token expired: ${res}`);
    if (res) {
        this.toastr.error('Session has expired, please log in again');
    }
    return res;
  }

}
