
import {throwError as observableThrowError,  Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtService } from './jwt.service';
import { catchError, map, switchMap, distinctUntilChanged } from 'rxjs/operators';
import { Player } from '../models/player';

const API_URL = environment.apiUrl;

@Injectable()
export class ApiService {

  private currentUserSubject = new BehaviorSubject<Player>(this.getStoredPlayer());
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new BehaviorSubject<boolean>(this.getStoredAuthState());
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) { }

  // Verify JWT in sessionStorage with server & load user's info.
  // This runs once on application startup
  populate() {
    // If JWT detected, attempt to get & store user's info
    if (this.jwtService.getAccessToken()) {
        this.getPlayer().subscribe(
            data => {
            }
        );
    } else {
        // Remove any potential remnants of previous auth state
        this.purgeAuth();
    }
  }

  getPlayer(): Observable<any> {
    return this
        .get('/auth/player')
        .pipe(
          map(data => {
              const currentPlayer = new Player().deserialize(data);
              this.setAuth(currentPlayer);
              return data;
          }
        )
      );
  }

  private setHeaders(refresh?: boolean): HttpHeaders {
    const headersConfig = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    };
    if (this.jwtService.getAccessToken() && !refresh) {
        headersConfig['Authorization'] = `Bearer ${this.jwtService.getAccessToken()}`;
    } else if (refresh && this.jwtService.getRefreshToken()) {
        headersConfig['Authorization'] = `Bearer ${this.jwtService.getRefreshToken()}`;
    }
    return new HttpHeaders(headersConfig);
  }

  refreshToken(): Observable<any> {
    console.log(`refreshToken(): entered`);
    const refreshData = {
        refresh_token: this.jwtService.getRefreshToken()
    };
    return this.postRefresh('/token/refresh', refreshData)
        .pipe(
          map(data => {
              const accessToken = data.access_token;
              this.setTokens(accessToken, refreshData.refresh_token);
              // let user = this.currentUserSubject.getValue();
              const player = JSON.parse(sessionStorage.getItem('stored-player'));
              player.access_token = accessToken;
              player.refresh_token = refreshData.refresh_token;
              this.setAuth(player);
              return data;
          }
        )
      );
  }

  setAuth(player: Player) {
    if (player) {
        // Set current user data into 
        sessionStorage.setItem('stored-player', JSON.stringify(player));
        this.currentUserSubject.next(this.getStoredPlayer());
        // Set isAuthenticated to true
        sessionStorage.setItem('gamma-auth-state', JSON.stringify(true));
        this.isAuthenticatedSubject.next(true);
    } else {
        this.purgeAuth();
    }
  }

  refreshUser() {
    const playerJson = sessionStorage.getItem('stored-player');
    const player = new Player().deserialize(JSON.parse(playerJson));
    this.get(`/players/${player.id}`).subscribe(
      (resp: Player) => {
        sessionStorage.setItem('stored-player', JSON.stringify(resp));
      },
      (err) => {
        console.log(`There was an error refreshing the player: ${JSON.stringify(err)}`);
      }
    );
  }

  purgeAuth() {
    // Remove JWT from session
    this.jwtService.destroyTokens();
    // // Set current user to an empty object
    sessionStorage.setItem('stored-player', JSON.stringify(new Player()));
    this.currentUserSubject.next(this.getStoredPlayer());
    // Set auth status to false
    sessionStorage.setItem('gamma-auth-state', JSON.stringify(false));
    this.isAuthenticatedSubject.next(false);
  }

  setTokens(accessToken: string, refreshToken: string) {
    this.jwtService.saveAccessToken(accessToken);
    this.jwtService.saveRefreshToken(refreshToken);
  }

  login(credentials): Observable<any> {
    console.log(`ApiService: login() called`);
    return this.http.post(`${API_URL}/login`, credentials)
      .pipe(
        map(
          (data: any) => {
            const accessToken = data.access_token;
            const refreshToken = data.refresh_token;
            this.setTokens(accessToken, refreshToken);
            return data;
          }
        )
      );
  }

  logout() {
    console.log(`ApiService: removing access token`);
    this.post('/logout/access').subscribe(
      (resp: Response) => {
        console.log(`Access token revoked`);
      },
      (err) => {
        console.log(`Error revoking access token: ${JSON.stringify(err)}`);
      },
      () => {
        console.log(`Finished revoking access token, removing from storage`);
        this.jwtService.destroyTokens();
      }
    );
  }

  private getStoredPlayer(): Player {
    const cached = sessionStorage.getItem('stored-player');
    if (cached) {
        return JSON.parse(cached);
    } else {
        return new Player();
    }
  }

  private getStoredAuthState(): boolean {
    const cached = sessionStorage.getItem('gamma-auth-state');
    if (cached) {
        return JSON.parse(cached);
    } else {
        return false;
    }
  }

  get(path: string): Observable<any> {
    // console.log('ApiService get(): starting...');
    return this.http.get(`${API_URL}${path}`, {headers: this.setHeaders()} );
  }

  post(path: string, data?: any): Observable<any> {
    // console.log(`apiService: post() with data: ${JSON.stringify(data)}`);
    return this.http.post(`${API_URL}${path}`, JSON.stringify(data), {headers: this.setHeaders()});
  }

  postRefresh(path: string, body: Object = {}): Observable<any> {
    console.log(`postRefresh() called on path ${path}`);
    return this.http.post(`${API_URL}${path}`, JSON.stringify(body), {headers: this.setHeaders(true)});
  }

  put(path: string, data: any): Observable<any> {
    return this.http.put(`${API_URL}${path}`, data, {headers: this.setHeaders()});
  }

  delete(path: string): Observable<any> {
    // console.log('ApiService get(): starting...');
    return this.http.delete(`${API_URL}${path}`, {headers: this.setHeaders()} );
  }

  uploadFile(path: string, data?: any): Observable<any> {
    // Flask will only process files if the Content-Type header is missing
    // for some reason, so we specifically omit it here
    const headersConfig = {
      // 'Content-Type': 'multipart/form-data',
      'Accept': 'application/json',
      'Authorization': `Bearer ${this.jwtService.getAccessToken()}`
    };
    const headers = new HttpHeaders(headersConfig);
    return this.http
      .post(`${API_URL}${path}`, data, {headers: headers});
  }

}
