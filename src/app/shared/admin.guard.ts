import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AdminGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log(`AuthGuard: Checking to see if player is an admin`);
        if (sessionStorage.getItem('stored-player')) {
            // logged in so return true
            console.log(`Player is logged in`)
            const player = JSON.parse(sessionStorage.getItem('stored-player'));
            if (player.team_admin === true) {
                console.log(`Player is an admin, we'll allow it`);
                return true;
            } else {
                console.log(`Player is not an admin, blocking`);
            }
        }

        // not logged in so redirect to login page
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}
