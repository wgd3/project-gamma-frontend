import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { JwtService } from './services/jwt.service';
import { Observable, BehaviorSubject, throwError as observableThrowError } from 'rxjs';
import { catchError, switchMap, filter, take } from '../../../node_modules/rxjs/operators';
import { ApiService } from './services/api.service';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

    private refreshTokenInProgress = false;
    // Refresh Token Subject tracks the current token, or is null if no token is currently
    // available (e.g. refresh pending).
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
        null
    );
    constructor(
        public jwt: JwtService,
        private apiService: ApiService
    ) {}

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError(error => {
                // We don't want to refresh token for some requests like login or refresh token itself
                // So we verify url and we throw an error if it's the case
                if ( request.url.includes('refresh') ||
                     request.url.includes('login') ||
                     request.url.includes('password')) {
                    // We do another check to see if refresh token failed
                    // In this case we want to logout user and to redirect it to login page
                    if (request.url.includes('refresh')) {
                        this.apiService.logout();
                    }

                    return observableThrowError(error);
                }

                // If error status is different than 401 we want to skip refresh token
                // So we check that and throw the error if it's the case
                if (error.status !== 401) {
                    return observableThrowError(error);
                }

                if (this.refreshTokenInProgress) {
                    // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
                    // – which means the new token is ready and we can retry the request again
                    return this.refreshTokenSubject.pipe(
                        filter(result => result !== null),
                        take(1),
                        switchMap(() => next.handle(this.addAuthenticationToken(request)))
                    );

                } else {
                    console.log(`token-interceptor :: Attempting to refresh access token`);
                    this.refreshTokenInProgress = true;

                    // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
                    this.refreshTokenSubject.next(null);

                    // Call auth.refreshAccessToken(this is an Observable that will be returned)
                    return this.apiService.refreshToken().pipe(
                        switchMap((token: any) => {
                            // When the call to refreshToken completes we reset the refreshTokenInProgress to false
                            // for the next time the token needs to be refreshed
                            this.refreshTokenInProgress = false;
                            this.refreshTokenSubject.next(token);

                            return next.handle(this.addAuthenticationToken(request));
                        }),
                        catchError((err: any) => {
                            console.log(`token-interceptor :: error during token refresh: ${JSON.stringify(err)}`);
                            this.refreshTokenInProgress = false;

                            this.apiService.logout();
                            return observableThrowError(error);
                        })
                    );
                }
            })
        );
    }

    addAuthenticationToken(request) {
        // Get access token from Local Storage
        const accessToken = this.jwt.getAccessToken();

        // If access token is null this means that user is not logged in
        // And we return the original request
        if (!accessToken) {
            return request;
        }

        // We clone the request, because the original request is immutable
        return request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.jwt.getAccessToken()}`
            }
        });
    }
}
