import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange, Output, EventEmitter } from '@angular/core';
import { Player } from '../models/player';
import { ApiService } from '../services/api.service';
import { PlayerClass } from '../models/player.class';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'gamma-player-card',
  templateUrl: './player-card.component.html',
  styleUrls: ['./player-card.component.css']
})
export class PlayerCardComponent implements OnInit, OnChanges {

  @Input() player: Player = new Player();
  // allows us to specify 'queue', 'compact', 'default'
  // queue = used on the mob screen, player queue, <=300px
  // compact = WIP
  // default = full size, all info card, >600px
  @Input() cardStyle = 'default';
  @Input() queuePosition: number;
  @Input() atKeyboard = false;
  @Output() dropPlayerSignal: EventEmitter<any> = new EventEmitter();
  // playerClass: PlayerClass = new PlayerClass();
  classPicUrl = '';

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.loadData();
    if (this.queuePosition === 0) {
      this.atKeyboard = true;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(`Change detected on player card`);
    this.handleQueueChange(changes);
  }

  private handleQueueChange(changes: SimpleChanges) {
    const position: SimpleChange = changes.queuePosition;
    if ((position !== undefined) && (!position.firstChange)) {
      console.log(`Position changed for ${this.player.username}: moved from ${position.previousValue} to ${position.currentValue}`);
      if (position.currentValue === 0) {
        this.atKeyboard = true;
      } else {
        this.atKeyboard = false;
      }
      const payload = {
        'queue_position': position.currentValue
      };
      this.apiService.put(`/players/${this.player.id}`, payload).subscribe((resp) => {
        console.log(`Resp for player queue update: ${JSON.stringify(resp)}`);
      });
    }
  }

  loadData() {
    console.log(`Loading card for player ${this.player.username}`);
    // using forkJoin to load all information at once
    forkJoin(
      this.apiService.get(`/players/${this.player.id}`),
      // this.apiService.get(`/classes/${this.player.class_id}`)
    ).subscribe(
      ([playerResp, classResp]) => {
        this.classPicUrl = playerResp._links.class_picture;
        // this.playerClass = classResp;
      },
      (err) => {
        console.log(`There was an error loading player card details: ${JSON.stringify(err)}`);
      },
      () => {
        // console.log(`Finished loading player's extra data!`);
      }
    );
  }

  get progBarPercent() {
    if (this.player.xp !== 0) {
      // console.log(`Player ${this.player.username} has ${this.player.xp} and needs ${this.player.next_level_xp}`);
      const perc = Math.floor(
        (this.player.xp / this.player.next_level_xp) * 100
      );
      return perc;
    } else {
      return 0;
    }
  }

  signalPlayerDrop(player_id: number) {
    console.log(`Sending signal to drop player ${player_id}`);
    this.dropPlayerSignal.next(player_id);
  }

}
