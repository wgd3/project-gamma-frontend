export class Message {
    id: number;
    created: Date;
    description: string;
    image: any;
}
