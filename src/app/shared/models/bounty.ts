export class Bounty {
    id: number;
    created: Date;
    name: string;
    description: string;
    xp_reward: number;
    issue_id: number;
    issue_iid: number;
    issue_url: string;
    team_id: number;
    project: string;
    project_url: string;
    project_id: number;
    complete: boolean;
    completed_on = Date;
    players: any;
}