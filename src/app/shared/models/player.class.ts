export class PlayerClass {

    id: string;
    name: string;
    description: string;
    pic_id: string;

    deserialize(input: any): PlayerClass {
        Object.assign(this, input);
        return this;
    }
}
