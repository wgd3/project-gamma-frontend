export class Team {
    id: number;
    created: string;
    name: string;
    gitlab_url: string;
    gitlab_hook_token: string;
    gitlab_private_token: string;
    gitlab_bounty_label: string;
    default_interval_length: number;
    players: any[];

    deserialize(input: any): Team {
        Object.assign(this, input);
        return this;
    }
}