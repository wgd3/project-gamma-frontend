import { Deserializable } from './deserializable.model';

export class Player implements Deserializable {
    id: number;
    created: Date;
    username: string;
    first_name: string;
    last_name: string;
    email: string;
    level: number;
    xp: number;
    all_time_xp: number;
    next_level_xp: number;
    team_admin: boolean;
    queue_position: number;
    gitlab_username: string;
    class_id: number;
    team_id: number;
    access_token: string;
    refresh_token: string;
    available_coins: number;
    confirmed: boolean;
    confirmed_on: Date;
    class_name: string;
    has_temp_password: boolean;
    coins: any;
    inventory: any;

    deserialize(input: any) {
        Object.assign(this, input);
        return this;
    }

    get full_name() { return `${this.first_name} ${this.last_name}`; }

    get profile_alert_list(): any[] {
        const alerts = [];
        if (this.has_temp_password === true) {
            alerts.push('Need to update your password');
        }
        if ((this.first_name === null) ||
            (this.last_name === null)) {
            alerts.push('Need to add your first/last name');
        }
        return alerts;
    }
}
