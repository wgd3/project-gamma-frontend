import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FeatherIconsPipe } from './feather-pipe';
import { PlayerCardComponent } from './player-card/player-card.component';
import { SortPipe } from './sortby.pipe';
import { MessageService } from './services/message.service';

@NgModule({
  imports: [
    CommonModule,
    NgbModule
  ],
  declarations: [
    FeatherIconsPipe,
    PlayerCardComponent,
    SortPipe
  ],
  exports: [
    FeatherIconsPipe,
    PlayerCardComponent,
    SortPipe
  ],
  providers: [MessageService]
})
export class SharedModule { }
