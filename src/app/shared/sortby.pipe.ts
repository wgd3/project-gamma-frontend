import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortBy', 
  pure: true
})
export class SortPipe implements PipeTransform {
  transform(array: Array<string>, args: string): Array<string> {
    // console.log(`sortBy pipe called with values: ${JSON.stringify(array)} and args: ${args}`);
    array.sort((a: any, b: any) => {
      if ( a[args] < b[args] ) {
        // console.log(`${a[args]} is less than ${b[args]}`);
        return -1;
      } else if ( a[args] > b[args] ) {
        // console.log(`${a[args]} is more than ${b[args]}`);
        return 1;
      } else {
        return 0;
      }
    });
    return array;
  }
}
