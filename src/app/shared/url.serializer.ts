import { UrlTree , DefaultUrlSerializer, UrlSerializer } from '@angular/router';

export class CleanUrlSerializer extends DefaultUrlSerializer {
    public parse(url: string): UrlTree {
        // tslint:disable-next-line:no-shadowed-variable
        function cleanUrl(url) {
            return url.replace(/\(|\)/g, ''); // for example to delete parenthesis
        }
        return super.parse(cleanUrl(url));
    }
}
