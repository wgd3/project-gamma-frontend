import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { JwtService } from './services/jwt.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private jwtService: JwtService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log(`AuthGuard: Checking to see if player is logged in`);
        if ((sessionStorage.getItem('stored-player')) && (!this.jwtService.isTokenExpired())) {
            // logged in so return true
            console.log(`Player is logged in`);
            return true;
        }

        // not logged in so redirect to login page
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}
