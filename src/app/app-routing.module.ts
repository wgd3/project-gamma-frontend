import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AdminGuard } from './shared/admin.guard';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TeamsComponent } from './dashboard/teams/teams.component';
import { PlayersComponent } from './dashboard/players/players.component';
import { LandingComponent } from './dashboard/landing/landing.component';
import { TeamComponent } from './team/team.component';
import { NewTeamComponent } from './login/newteam.component';
import { NewplayerComponent } from './login/newplayer.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './shared/auth.guard';
import { MobComponent } from './mob/mob.component';
import { InviteComponent } from './invite/invite.component';
import { BankComponent } from './dashboard/bank/bank.component';
import { MarketplaceComponent } from './marketplace/marketplace.component';
import { MarketplaceComponent as DashMarketplace } from './dashboard/marketplace/marketplace.component';
import { GiftComponent } from './dashboard/gift/gift.component';
import { BountiesComponent } from './bounties/bounties.component';
import { LevelComponent } from './dashboard/level/level.component';
import { BountiesComponent as DashBountyComponent } from './dashboard/bounties/bounties.component';
import { MobComponent as DashMobComponent } from './dashboard/mob/mob.component';
import { MediaComponent } from './dashboard/media/media.component';
import { ClassComponent } from './dashboard/class/class.component';
import { ResetComponent } from './login/reset.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'newteam', component: NewTeamComponent },
  { path: 'newplayer', component: NewplayerComponent},
  { path: 'invite/:slug', component: InviteComponent },
  { path: 'resetpassword/:slug', component: ResetComponent},
  {
    path: 'marketplace',
    component: MarketplaceComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'mobs',
    component: MobComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'bounties',
    component: BountiesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  { path: 'team', component: TeamComponent },
  {
    path: 'dashboard',
    canActivate: [AdminGuard],
    component: DashboardComponent,
    children: [
      { path: '', component: LandingComponent, outlet: 'dash' },
      { path: 'teams', component: TeamsComponent, outlet: 'dash' },
      { path: 'mobs', component: DashMobComponent, outlet: 'dash' },
      { path: 'players', component: PlayersComponent, outlet: 'dash' },
      { path: 'bank', component: BankComponent, outlet: 'dash' },
      { path: 'marketplace', component: DashMarketplace, outlet: 'dash' },
      { path: 'gifts', component: GiftComponent, outlet: 'dash' },
      { path: 'bounties', component: DashBountyComponent, outlet: 'dash' },
      { path: 'levels', component: LevelComponent, outlet: 'dash' },
      { path: 'media', component: MediaComponent, outlet: 'dash' },
      { path: 'classes', component: ClassComponent, outlet: 'dash' }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    {
      enableTracing: false,
      useHash: false,
      preloadingStrategy: PreloadAllModules
    }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
