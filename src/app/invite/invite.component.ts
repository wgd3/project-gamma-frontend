import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { ApiService } from '../shared/services/api.service';

@Component({
  selector: 'gamma-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.css']
})
export class InviteComponent implements OnInit, OnDestroy {

  slug: string;
  private sub: any;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.slug = params['slug'];

      this.checkInvitationSlug();
   });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  checkInvitationSlug() {
    console.log(`Checking slug ${this.slug}`);
    this.apiService.get(`/players/invite/check/${this.slug}`).subscribe(
      (resp) => {
        console.log(`Found team with slug!`);
        this.router.navigateByUrl('/login');
      },
      (err) => {
        console.log(`Could not find team with slug`);
      },
      () => {
        console.log(`Finished checking team slug`);
      }
    )
  }

}
