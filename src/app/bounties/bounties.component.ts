import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Player } from '../shared/models/player';
import { Team } from '../shared/models/team';
import { Bounty } from '../shared/models/bounty';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'gamma-bounties',
  templateUrl: './bounties.component.html',
  styleUrls: ['./bounties.component.css']
})
export class BountiesComponent implements OnInit {

  currentPlayer: Player = new Player();
  currentTeam: Team = new Team();
  bounties: Bounty[] = [];

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayer = player;
      }
    );
    this.loadData();
  }

  loadData() {
    forkJoin(
      this.apiService.get(`/teams/${this.currentPlayer.team_id}`),
      this.apiService.get('/bounties')
    ).subscribe(
      ([teamResp, bountyResp]) => {
        this.currentTeam = teamResp;
        this.bounties = bountyResp.items;
      }
    );
  }

  
}
