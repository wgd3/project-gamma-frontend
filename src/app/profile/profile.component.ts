import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { Player } from '../shared/models/player';
import { forkJoin } from 'rxjs';
import { PlayerClass } from '../shared/models/player.class';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef } from '../../../node_modules/@ng-bootstrap/ng-bootstrap';


/* TODO: https://stackoverflow.com/questions/42446434/how-can-i-access-the-select-method-from-ngbtabset-in-the-component-in-angular2/44940187#44940187 */
@Component({
  selector: 'gamma-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  // for getting player information from API
  currentPlayerRef: Player = new Player();
  // for tracking and displaying real-time player variables
  currentPlayer: Player = new Player();
  classPicUrl = '';
  playerClass: PlayerClass = new PlayerClass();

  editProfileForm: FormGroup;
  editProfileFormStatusMessage: string;
  showSpinner: boolean;

  passwordForm: FormGroup;
  passwordFormStatusMessage: string;

  constructor(
    private apiService: ApiService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.apiService.currentUser.subscribe(
      (player: Player) => {
        this.currentPlayerRef = player;
      }
    );
    this.loadPlayer();
    this.buildPasswordForm();
    // this.buildEditForm();
  }

  loadPlayer() {
    // using forkJoin to load all information at once
    forkJoin(
      this.apiService.get(`/players/${this.currentPlayerRef.id}`),
      this.apiService.get(`/classes/${this.currentPlayerRef.class_id}`)
    ).subscribe(
      ([playerResp, classResp]) => {
        this.currentPlayer = playerResp;
        this.classPicUrl = playerResp._links.class_picture;
        this.playerClass = classResp;
      },
      (err) => {
        console.log(`There was an error loading player's extra data: ${JSON.stringify(err)}`);
      },
      () => {
        console.log(`Finished loading player's extra data!`);
        this.buildEditForm();
      }
    );
  }

  get profile_alert_list(): any[] {
      const alerts = [];
      if (this.currentPlayer.has_temp_password === true) {
          alerts.push('Need to update your password');
      }
      if ((this.currentPlayer.first_name === null) ||
          (this.currentPlayer.last_name === null)) {
          alerts.push('Need to add your first/last name');
      }
      return alerts;
  }

  buildEditForm() {
    this.editProfileForm = this.fb.group({
      username: [null, [
        Validators.minLength(5),
        Validators.required
      ]],
      email: [null, [
        Validators.email,
        Validators.required
      ]],
      first_name: [null, [
        Validators.required,
        Validators.minLength(2)
      ]],
      last_name: [null, [
        Validators.required,
        Validators.minLength(2)
      ]]
    });
    this.editProfileForm.patchValue({
      username: this.currentPlayer.username,
      first_name: this.currentPlayer.first_name,
      last_name: this.currentPlayer.last_name,
      email: this.currentPlayer.email
    });
  }

  get fUsername() { return this.editProfileForm.get('username'); }
  get fEmail() { return this.editProfileForm.get('email'); }
  get fFirstName() { return this.editProfileForm.get('first_name'); }
  get fLastName() { return this.editProfileForm.get('last_name'); }

  open(template) {
    this.modalService.open(template).result.then(
      (result) => {},
      (result) => {
        // make sure form is reset
        this.editProfileForm.reset();
        this.editProfileFormStatusMessage = '';
        this.showSpinner = false;
        console.log(`Closed modal and reset form`);
      }
    );
  }

  submitEditForm() {
    if (this.editProfileForm.valid) {
      this.showSpinner = true;
      console.log(`Submitting new player data...`);
      const payload = {
        username: this.fUsername.value,
        email: this.fEmail.value,
        first_name: this.fFirstName.value,
        last_name: this.fLastName.value
      };
      this.apiService.put(`/players/${this.currentPlayer.id}`, payload).subscribe(
        (resp) => {
          console.log(`PUT edit resp: ${JSON.stringify(resp)}`);
          this.editProfileFormStatusMessage = 'Successfully updated your profile!';
          this.loadPlayer();
        },
        (err) => {
          console.log(JSON.stringify(err));
          this.editProfileFormStatusMessage = 'Error updating profile';
        },
        () => {
          this.showSpinner = false;
        }
      );
    }
  }

  buildPasswordForm() {
    this.passwordForm = this.fb.group({
      old_password: [null, [
        Validators.required
      ]],
      new_password: [null, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50),
      ]],
      matching_password: [null, [
        Validators.required
      ]]
    });
  }

  get fOldPassword() { return this.passwordForm.get('old_password'); }
  get fNewPassword() { return this.passwordForm.get('new_password'); }
  get fMatchingPassword() { return this.passwordForm.get('matching_password'); }

  submitPasswordForm() {
    if (this.passwordForm.valid === true) {
      if (this.fNewPassword.value === this.fMatchingPassword.value) {
        this.showSpinner = true;
        console.log(`Updating player password..`);
        this.apiService.post(`/players/resetpassword`, {
          'old_password': this.fOldPassword.value,
          'new_password': this.fNewPassword.value
        }).subscribe(
          resp => {
            console.log(`Updated password!`);
            this.passwordFormStatusMessage = 'Updated your password!';
            this.passwordForm.reset();
          },
          (err) => {
            console.log(`Error updating password: ${JSON.stringify(err)}`);
            this.passwordFormStatusMessage = 'Error updating password';
          },
          () => {
            this.showSpinner = false;
          }
        );
      } else {
        console.log(`Passwords do not match`);
        this.fNewPassword.setErrors({ 'notMatching': true });
        this.fMatchingPassword.setErrors({ 'notMatching': true });
        this.passwordFormStatusMessage = 'New passwords must match!';
      }
    } else {
      console.log(`Password form is not valid`);
    }
  }
}
