FROM node:9.4 as building

WORKDIR /app

COPY package*.json /app/

RUN npm install

COPY ./ /app/

ARG env

RUN /bin/bash -c "npm run-script build ---configuration=${env-production}"

# Only put compiled application in container with nginx to serve it
FROM nginx:1.13

COPY --from=building /app/dist/ /usr/share/nginx/html

COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf